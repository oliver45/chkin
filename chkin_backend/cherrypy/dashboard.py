#!/usr/bin/env python

import webapp2
import jinja2
import os
import json
import random
import datetime
# from google.appengine.ext import ndb
# from google.appengine.ext.ndb import stats
# from google.appengine.api import users
from collections import defaultdict
import csv
from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
# from google.appengine.ext import blobstore
# from google.appengine.ext.webapp import blobstore_handlers
import urllib

import info
import main

bin_num = 20
bin_size = {
'storage': 4 * 1024 * 1024,
'downloaded': 1024 * 1024,
'cost': 0.01,
}

mutiply = {
'daily': 1,
'weekly': 5,
'monthly': 20
}

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__), 'dashboard/templates')),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

Base = declarative_base()

class StatsPage(webapp2.RequestHandler):
    def get(self):
        contexts = {
        'logout_url': users.create_logout_url('/dashboard/')
        }
        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render(contexts))


class StatsCountsAPI(webapp2.RequestHandler):
    def post(self):
        return self.get()

    def get(self):
        base = datetime.datetime(1970, 1, 1).date()
        resp = []
        for ent in info.UserCount.summary():
            resp.append({'ts': int((ent.date - base).total_seconds() * 1000), 'count': ent.count})
        self.response.write(json.dumps({"result": True, "message": resp, "error_code": 0}))


class StatsRecordAPI(webapp2.RequestHandler):
    def post(self):
        return self.get()

    def get(self):
        kind_stats = stats.KindStat().query().filter(stats.KindStat.kind_name == "User").get()
        if kind_stats:
            count = kind_stats.count
        else:
            count = 0
        info.UserCount.log(count, datetime.datetime.now())
        self.response.write(json.dumps({"result": True, "message": count, "error_code": 0}))


class UsersPage(webapp2.RequestHandler):
    def get(self):
        contexts = {
        'logout_url': users.create_logout_url('/dashboard/')
        }
        template = JINJA_ENVIRONMENT.get_template('users.html')
        self.response.write(template.render(contexts))


class UsersListAPI(webapp2.RequestHandler):
    def post(self):
        return self.get()

    def get(self):
        self.response.write(
            json.dumps({"result": True, "message": info.UserInfo.summary(), "error_code": 0}, default=str))


class UsersCameraIncrementAPI(webapp2.RequestHandler):
    def post(self):
        return self.get()

    def get(self):
        key = self.request.get('key')
        rev = self.request.get('rev')

        @ndb.transactional(retries=10)
        def increment_camera(key, rev):
            rev_key = ndb.Key(urlsafe=key)
            obj = rev_key.get()
            if obj:
                # camera_count = main.Camera.query(main.Camera.owner==rev_key).count()
                # obj.camera_count = (camera_count if camera_count != None else 0) + (-1 if rev else 1)
                obj.camera_count = (obj.camera_count if obj.camera_count != None else 0) + (-1 if rev else 1)
                obj.put()
                return obj.camera_count
            else:
                # quick fix to get around NoneType object error
                return 0

        count = increment_camera(key, rev != '')
        self.response.write(
            json.dumps({"result": True, "message": {'key': key, 'count': count}, "error_code": 0}, default=str))


class UsersMediaIncrementAPI(webapp2.RequestHandler):
    def post(self):
        return self.get()

    def get(self):
        key = self.request.get('key')
        size = self.request.get('size')
        rev = self.request.get('rev')

        @ndb.transactional(retries=10)
        def increment_media(key, size, rev):
            rev_key = ndb.Key(urlsafe=key)
            obj = rev_key.get()
            obj.media_count = (obj.media_count if obj.media_count != None else 0) + (-1 if rev else 1)
            obj.storage_used = (obj.storage_used if obj.storage_used != None else 0) + (
            -int(size) if rev else int(size))
            obj.put()
            return obj.media_count, obj.storage_used

        count, sum = increment_media(key, size, rev != '')
        self.response.write(json.dumps(
            {"result": True, "message": {'key': key, 'size': size, 'count': count, 'sum': sum}, "error_code": 0},
            default=str))


class ProfilePage(webapp2.RequestHandler):
    def get(self):
        contexts = {
        'logout_url': users.create_logout_url('/dashboard/'),
        'key': self.request.get('key')
        }
        try:
            rev_key = ndb.Key(urlsafe=self.request.get('key'))
            user = rev_key.get()
            contexts['username'] = user.name
            contexts['camera_count'] = user.camera_count
            contexts['media_count'] = user.media_count
            contexts['storage_used'] = user.storage_used
        except:
            pass
        template = JINJA_ENVIRONMENT.get_template('profile.html')
        self.response.write(template.render(contexts))


def profile_usage(key):
    base = datetime.datetime(1970, 1, 1)
    data = defaultdict(lambda: {'storage': 0, 'downloaded': 0})

    for ent in info.UsageLog.history(key):
        data[ent.date]['storage'] += ent.storage if ent.storage != None else 0
        data[ent.date]['downloaded'] += ent.downloaded if ent.downloaded != None else 0

    now = datetime.datetime.now()
    end = datetime.datetime(now.year, now.month, now.day) - info.day
    start = end - info.month
    est = info.day

    resp = []
    while start + est <= end:
        date = start + est
        est += info.day
        resp.append({
        'ts': int((date - base).total_seconds() * 1000),
        'usage': {
        'storage': data[date]['storage'],
        'downloaded': data[date]['downloaded']
        },
        'cost': {
        'storage': info.calculate_cost(data[date]['storage'], 0),
        'downloaded': info.calculate_cost(0, data[date]['downloaded'])
        }
        })
    return resp


class ProfileHistoryAPI(webapp2.RequestHandler):
    def post(self):
        return self.get()

    def get(self):
        key = self.request.get('key')
        resp = profile_usage(key)
        self.response.write(json.dumps({"result": True, "message": resp, "error_code": 0}))


class CamerasPage(webapp2.RequestHandler):
    def get(self):
        contexts = {
        }
        template = JINJA_ENVIRONMENT.get_template('cameras.html')
        self.response.write(template.render(contexts))


class CamerasListAPI(webapp2.RequestHandler):
    def post(self):
        return self.get()

    def get(self):
        self.response.write(
            json.dumps({"result": True, "message": info.CameraInfo.summary(), "error_code": 0}, default=str))


class MediaPage(webapp2.RequestHandler):
    def get(self):
        contexts = {
        'logout_url': users.create_logout_url('/dashboard/')
        }
        template = JINJA_ENVIRONMENT.get_template('media.html')
        self.response.write(template.render(contexts))


class MediaListAPI(webapp2.RequestHandler):
    def post(self):
        return self.get()

    def get(self):
        self.response.write(
            json.dumps({"result": True, "message": info.MediaInfo.summary(), "error_code": 0}, default=str))


class MediaAjaxAPI(webapp2.RequestHandler):
    def post(self):
        return self.get()

    def get(self):
        key = self.request.get('key')
        col = ['', 'title', 'user', 'camera', 'record_date', 'duration', 'size']
        start = int(self.request.get('iDisplayStart'))
        length = int(self.request.get('iDisplayLength'))
        sort = col[int(self.request.get('iSortCol_0'))]
        dir = self.request.get('sSortDir_0')
        cursor = self.request.get('cursor')
        direction = self.request.get('direction')
        if key != '':
            rev_key = ndb.Key(urlsafe=key)
            user = rev_key.get()
            count = user.media_count
        else:
            key = None
            kind_stats = stats.KindStat().query().filter(stats.KindStat.kind_name == "VideoSegment").get()
            if kind_stats:
                count = kind_stats.count
            else:
                count = 0
        resp = info.MediaInfo.ajax(sort, dir, length, int(start / length) + 1, key)
        self.response.write(json.dumps(
            {
            "sEcho": self.request.get('sEcho'),
            "iTotalRecords": count,
            "iTotalDisplayRecords": count,
            "aaData": resp
            }
            , default=str))


class MediaProfileAPI(webapp2.RequestHandler):
    def post(self):
        return self.get()

    def get(self):
        self.response.write(
            json.dumps({"result": True, "message": info.MediaInfo.summary(self.request.get('key')), "error_code": 0},
                       default=str))


class MediaBlobListAPI(webapp2.RequestHandler):
    def post(self):
        return self.get()

    def get(self):
        self.response.write(
            json.dumps({"result": True, "message": info.MediaInfo.blob_list(), "error_code": 0}, default=str))


class MediaSetSizeAPI(webapp2.RequestHandler):
    def post(self):
        return self.get()

    def get(self):
        key = self.request.get('key')
        size = self.request.get('size')
        info.MediaInfo.set_size(key, size)
        self.response.write(json.dumps({"result": True, "message": "OK", "error_code": 0}, default=str))


class MediaServeHandler(blobstore_handlers.BlobstoreDownloadHandler):
    def get(self, resource):
        resource = str(urllib.unquote(resource))
        blob_info = blobstore.BlobInfo.get(resource)
        if blob_info == None:
            contexts = {
            'logout_url': users.create_logout_url('/dashboard/'),
            'resource': resource
            }
            template = JINJA_ENVIRONMENT.get_template('blob_missing.html')
            self.response.write(template.render(contexts))
        else:
            self.send_blob(blob_info)


class UsagePage(webapp2.RequestHandler):
    def get(self):
        contexts = {
        'logout_url': users.create_logout_url('/dashboard/')
        }
        template = JINJA_ENVIRONMENT.get_template('usage.html')
        self.response.write(template.render(contexts))


class UsageRecordsAPI(webapp2.RequestHandler):
    def post(self):
        return self.get()

    def get(self):
        base = datetime.datetime(1970, 1, 1)
        resp = []
        for ent in info.UsageLog.summary():
            storage = ent.storage if ent.storage else 0
            downloaded = ent.downloaded if ent.downloaded else 0
            cost = storage * info.storage_cost / 1073741824.0 + downloaded * info.downloaded_cost / 1073741824.0
            resp.append(
                {'date': int((ent.date - base).total_seconds() * 1000), 'type': ent.key.kind(), 'name': ent.name,
                 'cost': cost, 'storage': storage, 'downloaded': downloaded})
        self.response.write(json.dumps({"result": True, "message": resp, "error_code": 0}))


def build_histogram():
    histogram = defaultdict(lambda: defaultdict(lambda: defaultdict(int)))
    histogram["config"]["bin_num"] = bin_num
    for type in ['daily', 'weekly', 'monthly']:
        for kind in ['storage', 'downloaded', 'cost']:
            for bin in range(bin_num + 1):
                histogram[type][kind][bin * bin_size[kind] * mutiply[type]] = 0

    for ent in info.UsageLog.all():
        if ent.key.kind() != 'User':
            continue
        data = {}
        data['storage'] = ent.storage if ent.storage else 0
        data['downloaded'] = ent.downloaded if ent.downloaded else 0
        data['cost'] = info.calculate_cost(data['storage'], data['downloaded'])
        for kind in ['storage', 'downloaded', 'cost']:
            histogram[ent.type][kind][
                min(bin_num, int(data[kind] / bin_size[kind] * mutiply[ent.type])) * bin_size[kind] * mutiply[
                    ent.type]] += 1
    return histogram


class UsageHistogramAPI(webapp2.RequestHandler):
    def post(self):
        return self.get()

    def get(self):
        self.response.write(json.dumps({"result": True, "message": build_histogram(), "error_code": 0}))


class CVSAPI(webapp2.RequestHandler):
    def post(self, report):
        return self.get(report)

    def get(self, report):
        key = self.request.get('key')
        report, filename = report.split('/')

        self.response.headers['Content-Type'] = 'application/csv'
        writer = csv.writer(self.response.out)

        if report == 'users':
            writer.writerow(["User", "# of Cameras", "# of Media", "Storage Used", "$ Daily", "$ Weekly", "$ Monthly",
                             "Storage Daily", "Storage Weekly", "Storage Monthly", "Downloaded Daily",
                             "Downloaded Weekly", "Downloaded Monthly"])
            for ent in info.UserInfo.summary():
                writer.writerow([ent['id'], ent['camera_count'], ent['media_count'], ent['storage_used'],
                                 ent['usage']['storage']['daily'], ent['usage']['storage']['weekly'],
                                 ent['usage']['storage']['monthly'], ent['usage']['downloaded']['daily'],
                                 ent['usage']['downloaded']['weekly'], ent['usage']['downloaded']['monthly'],
                                 ent['cost']['daily'], ent['cost']['weekly'], ent['cost']['monthly']])
        elif report == 'cameras':
            writer.writerow(
                ["MAC Address", "User", "LAN IP", "LAN Port", "WAN IP", "WAN Port", "$ Daily", "$ Weekly", "$ Monthly",
                 "Storage Daily", "Storage Weekly", "Storage Monthly", "Downloaded Daily", "Downloaded Weekly",
                 "Downloaded Monthly"])
            for ent in info.CameraInfo.summary():
                writer.writerow(
                    [ent['id'], ent['owner'], ent['parameters']['internal_ip'], ent['parameters']['internal_port'],
                     ent['parameters']['external_ip'], ent['parameters']['external_port'],
                     ent['usage']['storage']['daily'], ent['usage']['storage']['weekly'],
                     ent['usage']['storage']['monthly'], ent['usage']['downloaded']['daily'],
                     ent['usage']['downloaded']['weekly'], ent['usage']['downloaded']['monthly'], ent['cost']['daily'],
                     ent['cost']['weekly'], ent['cost']['monthly']])
        elif report == 'profile':
            writer.writerow(["Timestamp", "Usage Storage", "Usage Downloaded", "Cost Storage", "Cost Downloaded"])
            for ent in sorted(profile_usage(key), key=lambda e: e['ts']):
                writer.writerow(
                    [datetime.datetime.fromtimestamp(ent['ts'] / 1000).strftime('%Y-%m-%d'), ent['usage']['storage'],
                     ent['usage']['downloaded'], ent['cost']['storage'], ent['cost']['downloaded']])
        elif report == 'stats':
            writer.writerow(["Timestamp", "User Count"])
            for ent in info.UserCount.summary():
                writer.writerow([ent.date.strftime('%Y-%m-%d'), ent.count])


class TestPage(webapp2.RequestHandler):
    def get(self):
        contexts = {
        }
        template = JINJA_ENVIRONMENT.get_template('test.html')
        self.response.write(template.render(contexts))


class TestPostAPI(webapp2.RequestHandler):
    def post(self):
        email = self.request.get('email')
        self.response.write(json.dumps({"result": True, "message": email, "error_code": 0}, default=str))


class TestGenCountAPI(webapp2.RequestHandler):
    def post(self):
        return self.get()

    def get(self):
        info.UserCount.clear()
        count = 0
        now = datetime.datetime.now()
        for diff in range(-100, 0):
            info.UserCount.log(count, now + datetime.timedelta(days=diff))
            count += random.randint(10, 100)
        self.response.write(json.dumps({"result": True, "message": "OK", "error_code": 0}))


class TestGenCount2API(webapp2.RequestHandler):
    def post(self):
        return self.get()

    def get(self):
        count = 0
        now = datetime.datetime.now()
        diff = -10
        info.UserCount.log(count, now + datetime.timedelta(days=diff))
        self.response.write(json.dumps({"result": True, "message": "OK", "error_code": 0}))


class TestGenDownloadedAPI(webapp2.RequestHandler):
    def post(self):
        return self.get()

    def get(self):
        count = 0
        now = datetime.datetime.now()
        base = datetime.datetime(now.year, now.month, now.day)

        key = ndb.Key(urlsafe='ahNzfnd0YW50aXNpLWNoa2luY2FtchELEgRVc2VyGICAgMCOk7oIDA')
        name = 'wtantisiriroj+chkin@gmail.com'

        for diff in range(-32, 0):
            info.UsageLog(date=base + datetime.timedelta(days=diff), type='daily', key=key, name=name,
                          downloaded=random.randint(1 * 1024 * 1024, 10 * 1024 * 1024)).put()
        self.response.write(json.dumps({"result": True, "message": "OK", "error_code": 0}))


class TestClearUsageAPI(webapp2.RequestHandler):
    def post(self):
        return self.get()

    def get(self):
        ndb.delete_multi(
            info.UsageLog.query().fetch(keys_only=True)
        )
        self.response.write(json.dumps({"result": True, "message": "OK", "error_code": 0}))


class UpdateCameraCountAPI(webapp2.RequestHandler):
    """update camera count via cron"""

    def post(self):
        return self.get()

    def get(self):
        users = main.User.query(main.User.deleted != True).fetch(keys_only=True)
        for owner in users:
            qry1 = main.Camera.query(
                main.Camera.owner == owner,
                main.Camera.deleted != True).fetch(keys_only=True)
            qry2 = main.Camera.query(
                main.Camera.owner == owner,
                main.Camera.claim_token != "").fetch(keys_only=True)
            owner = owner.get()
            owner.camera_count = set(qry1).intersection(qry2).__len__()
            owner.put()
        self.response.write(json.dumps({"result": True, "message": "OK", "error_code": 0}))


app = webapp2.WSGIApplication([
                                  ('/dashboard/', StatsPage),
                                  ('/dashboard/stats', StatsPage),
                                  ('/dashboard/stats/counts', StatsCountsAPI),
                                  ('/dashboard/stats/record', StatsRecordAPI),
                                  ('/dashboard/users', UsersPage),
                                  ('/dashboard/users/list', UsersListAPI),
                                  ('/dashboard/users/camera_increment', UsersCameraIncrementAPI),
                                  ('/dashboard/users/media_increment', UsersMediaIncrementAPI),
                                  ('/dashboard/profile', ProfilePage),
                                  ('/dashboard/profile/history', ProfileHistoryAPI),
                                  ('/dashboard/cameras', CamerasPage),
                                  ('/dashboard/cameras/list', CamerasListAPI),
                                  ('/dashboard/media', MediaPage),
                                  ('/dashboard/media/list', MediaListAPI),
                                  ('/dashboard/media/ajax', MediaAjaxAPI),
                                  ('/dashboard/media/profile', MediaProfileAPI),
                                  ('/dashboard/media/bloblist', MediaBlobListAPI),
                                  ('/dashboard/media/set_size', MediaSetSizeAPI),
                                  ('/dashboard/media/serve/([^/]+)?/.*', MediaServeHandler),
                                  ('/dashboard/usage', UsagePage),
                                  ('/dashboard/usage/records', UsageRecordsAPI),
                                  ('/dashboard/usage/histogram', UsageHistogramAPI),
                                  ('/dashboard/csv/(.*?)', CVSAPI),
                                  ('/dashboard/test', TestPage),
                                  ('/dashboard/test/post', TestPostAPI),
                                  ('/dashboard/test/gen_count', TestGenCountAPI),
                                  ('/dashboard/test/gen_count2', TestGenCount2API),
                                  ('/dashboard/test/downloaded_mock', TestGenDownloadedAPI),
                                  ('/dashboard/test/clear_usage', TestClearUsageAPI),
                                  ('/dashboard/update/camera_count', UpdateCameraCountAPI),
                              ], debug=True)
