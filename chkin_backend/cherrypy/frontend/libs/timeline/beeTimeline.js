/// <reference path="../jquery/jquery.d.ts" />
/// <reference path="../moment/moment.d.ts" />
/// <reference path="../vis_js/vis.d.ts" />
var BTL = (function () {
    function BTL(element, timeSpan) {
        this.items = [];
        this.vis_options = {
            margin: { item: 10 },
            height: 70,
            zoomable: false,
            'margin.axis': 2,
            minHeight: 60,
            showMajorLabels: false,
            stack: false,
            selectable: true,
            showCurrentTime: false,
            maxHeight: "65px"
        };
        this.cursor_screen_x = 0;
        this.time_per_pixel = 0;
        this.timelineUpdated = false;
        this.moveInProgress = false;
        this.outByUser = false;
        this.margin = 30;
        this.treshold = 10;
        this.initDone = false;
        this.element = element;
        this.timeSpan = timeSpan;
        this.init();
    }
    BTL.prototype.init = function () {
        var _this = this;
        this.items = [];
        var cursor = new beeItem("cursor", "<div id='cursor_content' class='cursor_content'><p></p></div>", new Date(2014, 10, 10), new Date(2014, 10, 10), "cursor");
        this.items.push(cursor);

        // Create a Timeline
        this.timeline = new vis.Timeline(this.element, this.items, this.vis_options);
        this.RegisterEvents();
        this.SetCursorTime(moment().toDate());
        this.SetTimeSpan(this.timeSpan.SPAN, this.timeSpan.COUNT);
        setTimeout(function () {
            _this.SetCursorTime(moment().toDate());
        }, 620);

        this.initDone = true;
    };

    BTL.prototype.SetCursorTime = function (time, update) {
        var _this = this;

        if (typeof update === "undefined") { update = false; }
        if (time === undefined || time === null || !this.isValidDate(time)) {
            return;
        }
        var updateCursor = false;
        if (!this.moveInProgress) {
            updateCursor = true;
        } else if (this.moveInProgress && update) {
            updateCursor = true;
        }

        if (updateCursor) {
            console.log("this.moveInProgress: ", this.moveInProgress, "update: ", update);
            if (!this.outByUser) {
                this.CheckCursorPosition();
            }
            ;
            var dataSet = this.timeline.itemsData;
            var cursor = dataSet.get("cursor");
            cursor.start = time;
            cursor.end = time;
            dataSet.update(cursor);
            $("#cursor_content>p").html(moment(time).format("hh:mm A"));
            $("#cursor_content").on("mousedown", function (e) {
                return _this.onMouseTimelineDown(e, _this);
            });
        }
    };

    BTL.prototype.GetCursorTime = function () {
        var dataSet = this.timeline.itemsData;
        var cursor = dataSet.get("cursor");
        return cursor.start;
    };

    BTL.prototype.AddItems = function (items) {
        var addList = [];
        var dataSet = this.timeline.itemsData;
        var presentItems = dataSet.getIds();
        items.forEach(function (item) {
            if (jQuery.inArray(item.id, presentItems) > -1) {
                dataSet.update(item);
            } else {
                addList.push(item);
            }
        });
        dataSet.add(addList);
    };
	
	BTL.prototype.RemoveItems = function () {
        var dataSet = this.timeline.itemsData;
		var cursor = dataSet.get("cursor");

		dataSet.clear();
		dataSet.add(cursor);
    };

    BTL.prototype.SetTimeSpan = function (span, count) {
        var end;
        var start;
        var spanData = {};
        spanData[span["str"]] = count;
        var duration = moment.duration(spanData);
        var treshhold = (duration.asSeconds() / 100) * this.treshold;
        if (!this.initDone) {
            end = moment();
            start = moment(end).subtract(count, span["str"]).add(treshhold, "seconds");
            end = moment(start).add(count, span["str"]);
        } else {
            start = this.timeline.getWindow().start;
            end = this.timeline.getWindow().end;
            var cursor = this.GetCursorTime();
            if (cursor > start && cursor < end) {
                end = cursor;
                start = moment(end).subtract(count, span["str"]).add(treshhold, "seconds");
                end = moment(start).add(count, span["str"]);
            } else {
                end = moment(this.timeline.getWindow().end);
                start = moment(end).subtract(count, span["str"]);
            }
        }
        this.timeline.setWindow(start.toDate(), end.toDate());

        var w = $(this.element).width();
        this.time_per_pixel = duration.asMilliseconds() / w;
    };

    BTL.prototype.GetTimeBorders = function () {
        return [this.timeline.getWindow().start, this.timeline.getWindow().end];
    };

    BTL.prototype.GetStageTimeLength = function () {
        return moment.duration(moment(this.timeline.getWindow().end).diff(moment(this.timeline.getWindow().start))).asMilliseconds();
    };

    BTL.prototype.PixelToTime = function (dif_pixel) {
        var time = dif_pixel * this.time_per_pixel;
        return time;
    };

    BTL.prototype.MoveCursorBy = function (pixel) {
        var time = this.PixelToTime(pixel);
        var currentCursor = this.GetCursorTime();
        var t = moment(currentCursor).add(time, "milliseconds");
        console.log("MoveCursorBy");
        this.SetCursorTime(t.toDate(), true);
    };

    BTL.prototype.CheckCursorPosition = function () {
        var start = this.timeline.getWindow().start;
        var end = this.timeline.getWindow().end;
        if (this.GetCursorTime() >= moment(end).subtract(this.PixelToTime(this.margin), "milliseconds").toDate()) {
            var dur = moment.duration(moment(end).diff(moment(start)));
            var treshhold = (dur.asSeconds() / 100) * this.treshold;
            var new_end = moment(end).add(treshhold, "seconds");
            var new_start = moment(start).add(treshhold, "seconds");
            this.timeline.setWindow(new_start, new_end);
        }
    };

    /**
    * Move start/end so cursor place will be according to treshold value
    * @constructor
    */
    BTL.prototype.MoveWindowToCursor = function (time) {
        time = time === undefined ? this.GetCursorTime() : time;
        var start = this.timeline.getWindow().start;
        var end = this.timeline.getWindow().end;
        var dur = moment.duration(moment(end).diff(moment(start)));
        var treshhold = (dur.asSeconds() / 100) * this.treshold;
        var new_end = moment(this.GetCursorTime()).add(treshhold, "seconds");
        var new_start = moment(new_end).subtract(dur.asSeconds(), "seconds");
        this.timeline.setWindow(new_start.toDate(), new_end.toDate());
    };

    BTL.prototype.isValidDate = function (time) {
        if (isNaN(time.getTime()) == false) {
            return true;
        }
        return false;
    };

    BTL.prototype.RegisterEvents = function () {
        var _this = this;
        $("#cursor_content").on("mousedown", function (e) {
            return _this.onMouseTimelineDown(e, _this);
        });

        $(document).on("MoveCursorBy", function (e) {
            var diff = e["px"] - _this.cursor_screen_x;
            _this.cursor_screen_x = e["px"];
            _this.MoveCursorBy(diff);
        });

        $("div.vispanel.center").on("mousedown", function (e) {
            _this.timelineUpdated = false;
        });

        $("div.vispanel.center").on("mouseup", function (e) {
            if (_this.timelineUpdated) {
                return;
            }
            var offset = $("div.vispanel.center").offset();
            var diff = e.pageX - offset.left;
            var start = _this.timeline.getWindow().start;
            //var time = moment(start).add(_this.PixelToTime(diff - _this.margin), "milliseconds");
			var time = moment(start).add(_this.PixelToTime(diff), "milliseconds");

            _this.SetCursorTime(time.toDate());
            var event = jQuery.Event("cursorChange");
            event["time"] = _this.GetCursorTime();
            $(document).trigger(event);
        });

        this.timeline.on("rangechange", function (e) {
            _this.timelineUpdated = true;
            $("#cursor_content>p").html(moment(_this.GetCursorTime()).format("hh:mm A"));
            $(document).trigger(jQuery.Event("timelineTimeChange"));
        });

        this.timeline.on("rangechanged", function (e) {
            $(document).trigger(jQuery.Event("timelineTimeChange"));
            var cursorTime = _this.GetCursorTime();
            if (cursorTime < _this.timeline.getWindow().start || cursorTime > _this.timeline.getWindow().end) {
                _this.outByUser = true;
            } else {
                _this.outByUser = false;
            }
        });
    };

    BTL.prototype.onMouseUp = function (e, _this) {
        _this.moveInProgress = false;
        console.log("moveInProgress", _this.moveInProgress);
        $(document).off("mouseup");
        $(document).off("mousemove");
        var event = jQuery.Event("cursorChange");
        event["time"] = _this.GetCursorTime();
        $(document).trigger(event);
    };

    BTL.prototype.onMouseMove = function (e) {
        var event = jQuery.Event("MoveCursorBy");
        event["px"] = e.pageX;
        $(document).trigger(event);
    };

    BTL.prototype.onMouseTimelineDown = function (e, _this) {
        e.stopPropagation();
        $(document).on("mouseup", function (e) {
            return _this.onMouseUp(e, _this);
        });
        $(document).on("mousemove", _this.onMouseMove);
        _this.moveInProgress = true;
        console.log("moveInProgress", _this.moveInProgress);
        _this.cursor_screen_x = e.pageX;
    };
    return BTL;
})();

/**
* BTL time span unit
*/
var TimeSpan = (function () {
    function TimeSpan(SPAN, COUNT) {
        this.SPAN = SPAN;
        this.COUNT = COUNT;
    }
    return TimeSpan;
})();

/**
* Alternative for enum (IDE problem)
*/
var SpanType = (function () {
    function SpanType() {
    }
    SpanType.MINUTE = { str: "minute", id: 1000 * 60 };
    SpanType.HOUR = { str: "hour", id: 1000 * 60 * 60 };
    SpanType.DAY = { str: "day", id: 1000 * 60 * 60 * 24 };
    SpanType.MONTH = { str: "month", id: 1000 * 60 * 60 * 24 * 30 };
    return SpanType;
})();

var beeItem = (function () {
    function beeItem(id, content, start, end, className) {
        this.id = id;
        this.content = content;
        this.start = start;
        this.end = end;
        this.className = className;
    }
    return beeItem;
})();
//# sourceMappingURL=beeTimeline.js.map
