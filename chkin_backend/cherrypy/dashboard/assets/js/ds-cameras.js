d3.json('/dashboard/cameras/list', function(data) {
    var cost_format = d3.format(",.4f");
    var count_format = d3.format(",.0f");
    var size_format = d3.format(",.2f");
    var formatDate = d3.time.format("%Y-%m-%d")
    var parseDate = d3.time.format("%Y-%m-%d").parse;
    $row = d3.select("#table-records").selectAll("tr")
        .data(data.message).enter()
        .append("tr")
    
    $row.append("td").append("input")
        .attr("type", "checkbox")
        .attr("class", "iCheck select-action")
        .attr("data-id", function(d) { return d.key })
    $row.append("td").html(function(d) {
        disp = d.deleted ? '<del>'+d.id+'</del>' : d.id
        return disp
    })
    $row.append("td").text(function(d) { return d.owner })
    $row.append("td").append("span")
        .attr("class", function(d){return (d.parameters.online == "true") ? "status-online": "status-offline"})
        .html(function(d) { 
        return (d.parameters.online == "true") ? "On" : "Off" })
    $row.append("td").text(function(d) { return d.parameters.internal_ip })
    $row.append("td").text(function(d) { return d.parameters.internal_port })
    $row.append("td").text(function(d) { return d.parameters.external_ip })
    $row.append("td").text(function(d) { return d.parameters.external_port })
    $row.append("td").text(function(d) { return cost_format(d.cost.daily) })
    $row.append("td").text(function(d) { return cost_format(d.cost.weekly) })
    $row.append("td").text(function(d) { return cost_format(d.cost.monthly) })
    $row.append("td").text(function(d) { return size_format(d.usage.storage.daily/1024.0/1024.0) })
    $row.append("td").text(function(d) { return size_format(d.usage.storage.weekly/1024.0/1024.0) })
    $row.append("td").text(function(d) { return size_format(d.usage.storage.monthly/1024.0/1024.0) })
    $row.append("td").text(function(d) { return size_format(d.usage.downloaded.daily/1024.0/1024.0) })
    $row.append("td").text(function(d) { return size_format(d.usage.downloaded.weekly/1024.0/1024.0) })
    $row.append("td").text(function(d) { return size_format(d.usage.downloaded.monthly/1024.0/1024.0) })
    $row.append("td").text(function(d) { 
        return (d.registered_since) ? formatDate(parseDate(d.registered_since)) : "NA"; })
    $row.append("td").text(function(d) { return (d.has_cvr_subscription)?"Yes":"No" })
    $row.append("td").text(function(d) { 
        return (d.subscription_end_time) ? formatDate(parseDate(d.subscription_end_time)) : "NA";
         })
    $row.append("td").text(function(d) { return d.multicamera_discount })
    $row.append("td").text(function(d) { return d.coupon_code })

    var unsortableColumns = [];
    $('#datatable-table').find('thead th').each(function(){
        if ($(this).hasClass( 'no-sort')){
            unsortableColumns.push({"bSortable": false});
        }
        else if ($(this).hasClass( 'numeric-comma-sort')){
            unsortableColumns.push({"sType": "numeric-comma"});
        } else {
            unsortableColumns.push(null);
        }
    });

    oTable = $("#datatable-table").dataTable({
        "sDom": "<'row table-top-control'<'col-md-6 per-page-selector'l<'action-toolbar'>><'col-md-6'f>r>t<'row table-bottom-control'<'col-md-6'i><'col-md-6'p>>",
        "oLanguage": {
            "sLengthMenu": "_MENU_ &nbsp; records per page"
        },
        "iDisplayLength": 50,
        "aaSorting": [[ 1, "desc" ]],
        "aoColumns": unsortableColumns,
        "aoColumnDefs": [
            { "bVisible": false, "aTargets": [ 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 ]}
        ],
        "bAutoWidth": false,
    });

    $(".dataTables_length select").select2({
        minimumResultsForSearch: 10
    });
    
    $("div.action-toolbar").html('&nbsp;&nbsp;<button type="button" class="btn btn-danger delete-btn">Delete</button>');
    $("div.action-toolbar").hide()
    
    $(".stats-radio").click(function() {
        vis = {
            'cost': [8, 9, 10],
            'storage': [11, 12, 13],
            'downloaded': [14, 15, 16],
            'subscription': [17, 18, 19, 20, 21]
        }
        shown = vis[$(this).data('id')];

        for (var ip_info=4; ip_info <= 7; ip_info++){
            if ($(this).data('id') == 'subscription') {
                oTable.fnSetColumnVis(ip_info, false);
            } else {
                oTable.fnSetColumnVis(ip_info, true);
            }
        }

        for (iCol = 8; iCol <= 21; iCol++) {
            oTable.fnSetColumnVis( iCol, shown.indexOf(iCol) != -1);
        }
    })

    var selected = {}
    var count = 0
    
    $(".iCheck").iCheck({
        checkboxClass: 'icheckbox_square-grey',
        radioClass: 'iradio_square-grey'
    });

    $(".select-action").on("ifChecked", function() {
        selected[$(this).data('id')] = true
        console.log('add', $(this).data('id'), selected, d3.keys(selected))
        count += 1
        $("div.action-toolbar").show()
    })
    $(".select-action").on("ifUnchecked", function() {
        delete selected[$(this).data('id')]
        console.log('del', $(this).data('id'), selected, d3.keys(selected))
        count -= 1
        if (count <= 0) {
            $("div.action-toolbar").hide()
        }
    })
    
    $(".delete-btn").click(function() {
        var submited = d3.keys(selected).length
        var done = 0
        d3.keys(selected).forEach(function(id) {
            console.log('Delete', id)
            $.ajax({
                type: "POST",
                url: '/remove_camera_from_user',
                data: { camera_id: id, admin_mode: true },
                success: function(resp) {
                    console.log('resp', resp)
                    if (resp.result) {
                        done += 1
                        if (done == submited) {
                            location.reload();
                        }
                    }
                    else {
                        console.log('error', resp, resp.message)
                        $("#alert-zone").append('\
    <div class="alert alert-danger">\
        <button type="button" class="close" data-dismiss="alert">×</button>\
        <strong><i class="fa fa-ban"></i>  '+resp.error_code+'</strong> - '+resp.message+'\
    </div>\
                        ')
                    }
                },
                dataType: "json"
            });
        })
    }) 
})
