
import os
import md5
from time import sleep
from requests import post, get




CAMERA_USERNAME = "jn"
CAMERA_PASS = "kralj"
USER_EMAIL_ADDRESS = "alexandarkordic@gmail.com"
USER_PASSWORD = "alex"


SAFE_INTERVAL = 0.3



class MediaFileSpec(object):
	def __init__(self, name, duration):
		self.name, self.duration = name, duration

class DatabaseState:
	def __init__(self, host, port):
		self.host, self.port = host, port

	def run(self):
		self.user_and_camera()
		self.testdata()

	def post(self, *a, **kw):
		sleep(SAFE_INTERVAL)
		allowed_errors = kw.pop("allowed_errors", [])
		self.response = post(*a, **kw)
		try:
			self.r = self.response.json()
		except:
			print "error: {0}".format(str(self.response.content))
			raise
		if self.r["error_code"] != 0:
			if self.r["error_code"] in allowed_errors:
				print "[post] {0} data={2} allowed {1} ".format(a[0], str(self.response.content), kw.get("data", {}))
			else:
				raise Exception("error: {0}".format(str(self.response.content)))
		else:
			print "[post] {0} data={2} success {1} ".format(a[0], str(self.response.content), kw.get("data", {}))

	def get(self, *a, **kw):
		sleep(SAFE_INTERVAL)
		self.response = post(*a, **kw)
		try:
			self.r = self.response.json()
		except:
			print "error: {0}".format(str(self.response.content))
			raise
		if self.r["error_code"] != 0:
			raise Exception("error: {0}".format(str(self.response.content)))

	def user_and_camera(self):
		self.post("http://{host}:{port}/register_device".format(host=self.host, port=self.port), data={"name" : CAMERA_USERNAME, "password" : CAMERA_PASS,}, allowed_errors=(10,))
		self.post("http://{host}:{port}/register_user".format(host=self.host, port=self.port), data={"name" : USER_EMAIL_ADDRESS, "password" : USER_PASSWORD,}, allowed_errors=(10,))
		self.post("http://{host}:{port}/chl".format(host=self.host, port=self.port), data={"name" : CAMERA_USERNAME,})
		response = CAMERA_PASS + self.r["chl"]
		response = md5.new(response).hexdigest()
		self.post("http://{host}:{port}/auth".format(host=self.host, port=self.port), data={"name" : CAMERA_USERNAME, "response" : response,})
		self.key = self.r["key"] ## this is camera key - now logged in as camera
		self.post("http://{host}:{port}/claim_device_request".format(host=self.host, port=self.port), data={"user_email_address" : USER_EMAIL_ADDRESS, "key" : self.key}, allowed_errors=(5,))
		if self.r["error_code"] == 0:
			activate_url = raw_input("paste url from email please:")
			self.get(activate_url)

	def upload_file(self, dir, name, duration, utc_timestamp):
		self.post("http://{host}:{port}/segment_upload_request".format(host=self.host, port=self.port), data={"key" : self.key,})
		url = self.r['upload_url']
		video_file_name = "{0}.{1}".format(name, self.VIDEO_EXTENSION)
		image_file_name = "{0}.{1}".format(name, "jpg")
		self.post(url, 
			data={
			"key" : self.key,
			"utc_start" : str(utc_timestamp),
			"duration" : "%.2f" % duration,
			"chkin_version" : "0.0.13",
			"video_width" : "900",
			"video_height" : "720",
			"title" : "earth",
			"description" : "globe",
			"video_name" : video_file_name,
			"thumbnail_width" : "450",
			"thumbnail_height" : "360",
			},
			files={
			"video_data" : open( os.path.join(dir, video_file_name), "rb"),
			"thumbnail_data" : open( os.path.join(dir, image_file_name), "rb"),
			})
		print "file", name, "uploaded"

	def _is_media_missing(self, segment):
		for media in self.segments:
			if media["video_name"] == segment.name: return False
		return True

	VIDEOS_START_TS = 1403697771.0
	VIDEO_EXTENSION = "flv"
	def testdata(self):
		self.post("http://{host}:{port}/own_segments".format(host=self.host, port=self.port), data={"key" : self.key,})
		self.segments = self.r["segments"]
		dir = os.path.abspath(os.path.dirname(__file__))
		files = [
			MediaFileSpec("output_7", 6.98),
			MediaFileSpec("output_8", 6.85),
			MediaFileSpec("output_9", 7.11),
			MediaFileSpec("output_10", 6.86),
			MediaFileSpec("output_11", 7.07),
			MediaFileSpec("output_12", 7.0),
			MediaFileSpec("output_13", 6.98),
			MediaFileSpec("output_14", 6.98),
			MediaFileSpec("output_15", 6.97),
			MediaFileSpec("output_16", 6.97),
			MediaFileSpec("output_17", 0.95),
		]
		ts = self.VIDEOS_START_TS
		for f in files:
			f.ts = ts
			ts += f.duration

		files = filter(self._is_media_missing, files)

		print "uploading {0} files ...".format(len(files))
		for f in files:
			self.upload_file(dir, f.name, f.duration, f.ts)



if __name__ == "__main__":
	# test = DatabaseState("chkincam.appspot.com", 80)
	test = DatabaseState("127.0.0.1", 8080)
	test.run()

