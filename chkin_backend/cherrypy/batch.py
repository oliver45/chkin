import webapp2
from mapreduce.third_party import pipeline
from mapreduce import mapreduce_pipeline
from mapreduce import operation as op
import logging
import main
import info
from google.appengine.ext import ndb
from google.appengine.ext import blobstore
import datetime
from collections import defaultdict
import os

DEV = os.environ['SERVER_SOFTWARE'].startswith('Development')

if DEV:
	SHARDS = 1
else:
	SHARDS = 4


def propagate(entity):
	owner = entity.owner.get()
	if owner:
		owner.name = entity.name
		owner.put()

class NamePipeline(pipeline.Pipeline):
	def run(self, *args, **kwargs):
		mapper_params = {
			"entity_kind": "main.Credentials",
		}
		yield mapreduce_pipeline.MapperPipeline(
			"Propagate names",
			handler_spec="batch.propagate",
			input_reader_spec="mapreduce.input_readers.DatastoreInputReader",
			params=mapper_params,
			shards=SHARDS)

def retrieve_user(entity):
	if entity.owner:
		owner = entity.owner.get()
		if owner:
			entity.user = owner.name
			entity.put()

class CameraPipeline(pipeline.Pipeline):
	def run(self, *args, **kwargs):
		mapper_params = {
			"entity_kind": "main.Camera",
		}
		yield mapreduce_pipeline.MapperPipeline(
			"Retrieve usernames",
			handler_spec="batch.retrieve_user",
			input_reader_spec="mapreduce.input_readers.DatastoreInputReader",
			params=mapper_params,
			shards=SHARDS)

def group_by_camera(entity):
	if entity.owner:
		yield (entity.owner.id(), entity.key.id())

def name_by_camera(key, values):
	camera_key = ndb.Key(main.Camera, int(key))
	camera = camera_key.get()
	if camera:
		video_keys = map(lambda v: ndb.Key(main.VideoSegment, int(v)), values)
		video = ndb.get_multi(video_keys)
		for entity in video:
			entity.camera = camera.name
			entity.username = camera.user
			entity.user = camera.owner
		ndb.put_multi(video)

class MediaPipeline(pipeline.Pipeline):
	def run(self, *args, **kwargs):
		mapper_params = {
			"entity_kind": "main.VideoSegment",
		}
		yield mapreduce_pipeline.MapreducePipeline(
			"Propagate video usernames and cameras",
			mapper_spec="batch.group_by_camera",
			reducer_spec="batch.name_by_camera",
			input_reader_spec="mapreduce.input_readers.DatastoreInputReader",
			mapper_params=mapper_params,
			shards=SHARDS)

def retrieve_size(entity):
	if entity.video_data_blobkey:
		info = blobstore.BlobInfo.get(entity.video_data_blobkey)
		if info:
			entity.size = info.size
			entity.put()

class SizePipeline(pipeline.Pipeline):
	def run(self, *args, **kwargs):
		mapper_params = {
			"entity_kind": "main.VideoSegment",
		}
		yield mapreduce_pipeline.MapperPipeline(
			"Retrieve sizes",
			handler_spec="batch.retrieve_size",
			input_reader_spec="mapreduce.input_readers.DatastoreInputReader",
			params=mapper_params,
			shards=SHARDS)


def group_by_user(entity):
	if entity.user and entity.owner:
		yield (entity.user.id(), '%s|%s' % (entity.owner.id(), entity.size if entity.size else 0))

def user_stats(key, values):
	user_key = ndb.Key(main.User, int(key))
	user = user_key.get()
	if user:
		uniq = set()
		sum = 0
		count = 0
		for record in values:
			camera_id, size = record.split('|')
			uniq.add(camera_id)
			size = int(size)
			count += 1
			sum += size
		user.camera_count = len(uniq)
		user.media_count = count
		user.storage_used = sum
		user.put()

class StatsPipeline(pipeline.Pipeline):
	def run(self, *args, **kwargs):
		mapper_params = {
			"entity_kind": "main.VideoSegment",
		}
		yield mapreduce_pipeline.MapreducePipeline(
			"Count camera and video",
			mapper_spec="batch.group_by_user",
			reducer_spec="batch.user_stats",
			input_reader_spec="mapreduce.input_readers.DatastoreInputReader",
			mapper_params=mapper_params,
			shards=SHARDS)


def set_deleted(entity):
	if entity.deleted != True:
		entity.deleted = None
		entity.put()

class DeletedPropertyPipeline(pipeline.Pipeline):
	def run(self, *args, **kwargs):
		mapper_params = {
			"entity_kind": "main.User",
		}
		yield mapreduce_pipeline.MapperPipeline(
			"Set deleted property",
			handler_spec="batch.set_deleted",
			input_reader_spec="mapreduce.input_readers.DatastoreInputReader",
			params=mapper_params,
			shards=SHARDS)

		mapper_params = {
			"entity_kind": "main.Camera",
		}
		yield mapreduce_pipeline.MapperPipeline(
			"Set deleted property",
			handler_spec="batch.set_deleted",
			input_reader_spec="mapreduce.input_readers.DatastoreInputReader",
			params=mapper_params,
			shards=SHARDS)

		mapper_params = {
			"entity_kind": "main.VideoSegment",
		}
		yield mapreduce_pipeline.MapperPipeline(
			"Set deleted property",
			handler_spec="batch.set_deleted",
			input_reader_spec="mapreduce.input_readers.DatastoreInputReader",
			params=mapper_params,
			shards=SHARDS)


def get_downloaded(entity):
	if entity.user_key:
		yield ('%s|%s|%s' % (entity.user_key.urlsafe(), entity.timestamp.date(), entity.username), entity.size)
	if entity.camera_key:
		yield ('%s|%s|%s' % (entity.camera_key.urlsafe(), entity.timestamp.date(), entity.camera), entity.size)

def user_downloaded(key, values):
	ent_key, date_str, name = key.split('|', 2)
	rev_key = ndb.Key(urlsafe=ent_key)
	date = datetime.datetime.strptime(date_str, '%Y-%m-%d')

	total = 0
	for v in values:
		if v != 'None':
			try:
				value = int(v)
			except:
				value = 0
			total += value

	ent = info.UsageLog(date = date, type = 'daily', key = rev_key, name = name, downloaded = total)
	ent.put()

class DownloadPipeline(pipeline.Pipeline):
	def run(self, *args, **kwargs):
		if 'date' in kwargs and kwargs['date'] != '':
			start = datetime.datetime.strptime(kwargs['date'], '%Y-%m-%d')
			end = start + datetime.timedelta(1)
			mapper_params = {
				"input_reader": {
					"entity_kind": "info.ViewLog",
					"filters": [
						("timestamp", ">=", start), 
						("timestamp", "<", end)
					]
				},
			}
		else:
			mapper_params = {
				"entity_kind": "info.ViewLog",
			}

		yield mapreduce_pipeline.MapreducePipeline(
			"Compute downloaded bw",
			mapper_spec="batch.get_downloaded",
			reducer_spec="batch.user_downloaded",
			input_reader_spec="mapreduce.input_readers.DatastoreInputReader",
			mapper_params=mapper_params,
			shards=SHARDS)
			
def get_storage(entity):
	if entity.user:
		yield ('%s|%s|%s' % (entity.user.urlsafe(), entity.utc_start.date(), entity.username), entity.size)
	if entity.owner:
		yield ('%s|%s|%s' % (entity.owner.urlsafe(), entity.utc_start.date(), entity.camera), entity.size)

def user_storage(key, values):
	ent_key, date_str, name = key.split('|', 2)
	rev_key = ndb.Key(urlsafe=ent_key)
	date = datetime.datetime.strptime(date_str, '%Y-%m-%d')

	total = 0
	for v in values:
		if v != 'None':
			try:
				value = int(v)
			except:
				value = 0
			total += value

	ent = info.UsageLog(date = date, type = 'daily', key = rev_key, name = name, storage = total)
	ent.put()

class StoragePipeline(pipeline.Pipeline):
	def run(self, *args, **kwargs):
		if 'date' in kwargs and kwargs['date'] != '':
			start = datetime.datetime.strptime(kwargs['date'], '%Y-%m-%d')
			end = start + datetime.timedelta(1)
			mapper_params = {
				"input_reader": {
					"entity_kind": "main.VideoSegment",
					"filters": [
						("utc_start", ">=", start), 
						("utc_start", "<", end)
					]
				},
			}
		else:
			mapper_params = {
				"entity_kind": "main.VideoSegment",
			}

		yield mapreduce_pipeline.MapreducePipeline(
			"Compute downloaded bw",
			mapper_spec="batch.get_storage",
			reducer_spec="batch.user_storage",
			input_reader_spec="mapreduce.input_readers.DatastoreInputReader",
			mapper_params=mapper_params,
			shards=SHARDS)

class DailyStatsPipeline(pipeline.Pipeline):
	def run(self, *args, **kwargs):
		if 'date' in kwargs and kwargs['date'] != '':
			start = datetime.datetime.strptime(kwargs['date'], '%Y-%m-%d')
			end = start + datetime.timedelta(1)
			mapper_params = {
				"input_reader": {
					"entity_kind": "info.ViewLog",
					"filters": [
						("timestamp", ">=", start), 
						("timestamp", "<", end)
					]
				},
			}
		else:
			mapper_params = {
				"entity_kind": "info.ViewLog",
			}

		yield mapreduce_pipeline.MapreducePipeline(
			"Compute downloaded bw",
			mapper_spec="batch.get_downloaded",
			reducer_spec="batch.user_downloaded",
			input_reader_spec="mapreduce.input_readers.DatastoreInputReader",
			mapper_params=mapper_params,
			shards=SHARDS)

		if 'date' in kwargs and kwargs['date'] != '':
			start = datetime.datetime.strptime(kwargs['date'], '%Y-%m-%d')
			end = start + datetime.timedelta(1)
			mapper_params = {
				"input_reader": {
					"entity_kind": "main.VideoSegment",
					"filters": [
						("utc_start", ">=", start), 
						("utc_start", "<", end)
					]
				},
			}
		else:
			mapper_params = {
				"entity_kind": "main.VideoSegment",
			}

		yield mapreduce_pipeline.MapreducePipeline(
			"Compute downloaded bw",
			mapper_spec="batch.get_storage",
			reducer_spec="batch.user_storage",
			input_reader_spec="mapreduce.input_readers.DatastoreInputReader",
			mapper_params=mapper_params,
			shards=SHARDS)

def group_by_entity(entity):
	yield ('%s|%s' % (entity.key.urlsafe(), entity.name), '%s|%s|%s' % (entity.date.date(), entity.storage, entity.downloaded))

day = datetime.timedelta(1)
week = datetime.timedelta(7)
month = datetime.timedelta(30)

def monthly_stat(key, values):
	now = datetime.datetime.now()
	print now
	base = datetime.datetime(now.year, now.month, now.day) - day
	ent_key, name = key.split('|', 1)
	rev_key = ndb.Key(urlsafe=ent_key)
	daily_stat_storage = 0
	daily_stat_downloaded = 0
	weekly_stat_storage = 0
	weekly_stat_downloaded = 0
	monthly_stat_storage = 0
	monthly_stat_downloaded = 0
	for value in values:
		date_str, storage, downloaded = value.split('|')
		date = datetime.datetime.strptime(date_str, '%Y-%m-%d')
		if date <= base:
			storage = int(storage) if storage != 'None' else 0
			downloaded = int(downloaded) if downloaded != 'None' else 0
			if date + day > base:
				daily_stat_storage += storage
				daily_stat_downloaded += downloaded
			
			if date + week > base:
				weekly_stat_storage += storage
				weekly_stat_downloaded += downloaded
			
			if date + month > base:
				monthly_stat_storage += storage
				monthly_stat_downloaded += downloaded
			
	ent = info.UsageLog(date = base, type = 'weekly', key = rev_key, name = name, storage = weekly_stat_storage, downloaded = weekly_stat_downloaded)
	ent.put()

	ent = info.UsageLog(date = base, type = 'monthly', key = rev_key, name = name, storage = monthly_stat_storage, downloaded = monthly_stat_downloaded)
	ent.put()
	
	storage = main.Statistics(daily = daily_stat_storage, weekly = weekly_stat_storage, monthly = monthly_stat_storage)
	downloaded = main.Statistics(daily = daily_stat_downloaded, weekly = weekly_stat_downloaded, monthly = monthly_stat_downloaded)
	usage = main.Usage(storage = storage, downloaded = downloaded)
	ent = rev_key.get()
	ent.usage = usage
	ent.put()


# class MonthlyStatsPipeline(object):
# 	def start(self):
# 		print 'MonthlyStatsPipeline'
# 		inter = defaultdict(list)
# 		for entity in info.UsageLog.query().filter(info.UsageLog.type == 'daily'):
# 			for resp in group_by_entity(entity):
# 				inter[resp[0]].append(resp[1])
# 		
# 		for key in inter:
# 			ret = monthly_stat(key, inter[key])
# 			if ret != None:
# 				for resp in ret:
# 					print resp

class MonthlyStatsPipeline(pipeline.Pipeline):
	def run(self, *args, **kwargs):
		now = datetime.datetime.now()
		end = datetime.datetime(now.year, now.month, now.day) - day
		start = end - month
		mapper_params = {
			"input_reader": {
				"entity_kind": "info.UsageLog",
				"filters": [
					("type", "=", "daily"),
					("date", ">=", start), 
					("date", "<=", end)
				]
			},
		}

		yield mapreduce_pipeline.MapreducePipeline(
			"Compute monthly stats",
			mapper_spec="batch.group_by_entity",
			reducer_spec="batch.monthly_stat",
			input_reader_spec="mapreduce.input_readers.DatastoreInputReader",
			mapper_params=mapper_params,
			shards=SHARDS)


class BatchHandler(webapp2.RequestHandler):
	def get(self, job):
		if job == 'name':
			pipeline = NamePipeline()
		elif job == 'camera':
			pipeline = CameraPipeline()
		elif job == 'media':
			pipeline = MediaPipeline()
		elif job == 'size':
			pipeline = SizePipeline()
		elif job == 'stats':
			pipeline = StatsPipeline()
		elif job == 'deleted':
			pipeline = DeletedPropertyPipeline()
		elif job == 'downloaded':
			pipeline = DownloadPipeline()
		elif job == 'storage':
			pipeline = StoragePipeline()
		elif job == 'daily_stats':
			date = self.request.get('date')
			if date == '':
				date = (datetime.datetime.now() - datetime.timedelta(1)).strftime('%Y-%m-%d')
			pipeline = DailyStatsPipeline(date=date)
		elif job == 'monthly_stats':
			pipeline = MonthlyStatsPipeline()
# 			pipeline.start()
# 			return
			
		pipeline.start()
		redirect_url = "%s/status?root=%s" % (pipeline.base_path, pipeline.pipeline_id)
		self.redirect(redirect_url)




app = webapp2.WSGIApplication([
	('/batch/(.*?)', BatchHandler),
], debug=True)