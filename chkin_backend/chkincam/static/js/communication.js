var test_key = "ce8b8989-4ea1-4863-a3ff-f7d94bf19657";

/**
 * Know-how to get data from server
 */
var Communication = (function(){

    /**
     * Send current account data to server
     * @param general_data
     * @param show_notify
     */
    function set_general_account_params(general_data, show_notify){
        var url = '/set_account_infos';
        POST(url, general_data, show_notify);
    }

    /**
     * Communicate with server and get all cameras that user has right to see
     * @param callback
     * @param show_notify
     */
    function get_user_cameras(callback_success, callback_fail, show_notify){
        //var url = "http://chkincam.appspot.com//user_cameras?key=" + test_key;
        var url = "/user_cameras";
		var key = CookieUtils.readCookie("key");

		$.ajax(DB_SERVER_URL + url + "?key=" + key, {type: "GET", dataType: "json",
            success: function (data, textStatus, jqXHR) {
                if(data['message'] === "OK" && data['error_code'] === 0){
                    if(show_notify){notify("Data saved successfully", "success");}
                    callback_success(data);
                }
                else{
                    $.get(DB_SERVER_URL + '/session_expired?key=' + key, function(data){
                        if (! JSON.parse(data)['result']) {
                            setTimeout(function(){
                                notify("Session has Expired. Please login again", "error");
                            }, 300);                            
                            callback_fail();
                        } else {
                            notify("Received invalid data from server, please try later again", "success");
                            console.log("Received invalid data from server, please try later again", data);
                            //callback_fail();
                        }
                    });                    

                }
            },
            error: function (data, textStatus, jqXHR) {
                notify("Server error", "error");
                console.log("server error: ", data)
				//callback_fail();
            }

        }).fail(function() {
            notify("Server unavailable !!!", "error");
			//callback_fail();
        });
    }

    /**
     * Communicate with server and return all aailable data about given camera
     * @param camera_id
     * @param callback
     * @param show_notify
     */
    function get_data_for_camera(camera_id, callback, show_notify){
        //var url = 'http://chkincam.appspot.com/camera_segments?id={{CAM_ID}}&key=' + test_key;
        var url = '/camera_segments?id={{CAM_ID}}';
        url = url.replace('{{CAM_ID}}', camera_id);
        GET(url, "", show_notify, callback, camera_id);
    }

    /**
     * Get general setup data from server for given camera
     * @param camera_id
     * @param callback
     * @param show_notify
     */
    function get_general_camera_params(camera_id, callback, show_notify){
        //var url = 'http://chkincam.appspot.com/get_camera_state?camera_id={{CAM_ID}}&key=' + test_key;
        var url = '/get_camera_state?camera_id={{CAM_ID}}';
        url = url.replace('{{CAM_ID}}', camera_id);
        GET(url, "", show_notify, callback, camera_id);
    }

    /**
     * Send general setup data for given camera to server
     * @param camera_id
     * @param general_data
     * @param show_notify
     */
    function set_general_camera_params(camera_id, general_data, show_notify){
//        var url = 'http://chkincam.appspot.com/set_camera_state?camera_id={{CAM_ID}}&key=' + test_key;
        var url = '/set_camera_state?camera_id={{CAM_ID}}';
        url = url.replace('{{CAM_ID}}', camera_id);
        POST(url, general_data, show_notify);
    }

    function get_camera_settings(camera_id, callback, show_notify){
//        var url = "http://chkincam.appspot.com/set_camera_state?camera_id={{CAM_ID}}&key=" + test_key;
        var url = '/get_camera_state?camera_id={{CAM_ID}}';
        url = url.replace('{{CAM_ID}}', camera_id);
        GET(url, "", show_notify, callback, camera_id);
    }

    function set_camera_on_off(camera_id, isOn, show_notify){

//        var url = "http://chkincam.appspot.com/send_command?camera_id={{CAM_ID}}&payload={'action':'{{ACTION}}'}&collapse_key=None&expiry_in_seconds=300.0&key=" + test_key;
        var url = "/send_command?camera_id={{CAM_ID}}&payload={'action':'{{ACTION}}'}&collapse_key=None&expiry_in_seconds=300.0";

        url = url.replace("{{ACTION}}", isOn ? "app.chkin.START" : "app.chkin.SHUTDOWN");
        url = url.replace("{{CAM_ID}}", camera_id);
        console.log('set url: ', camera_id, isOn, url);
        GET(url, "", show_notify, function(){}, camera_id);
    }

	function delete_camera_confirmed() {
//		var url = "http://chkincam.appspot.com/remove_camera_from_user?camera_id={{CAM_ID}}&payload={'action':'{{ACTION}}'}&collapse_key=None&expiry_in_seconds=300.0&key=" + test_key;
		var url = "/remove_camera_from_user";
		data = {camera_id: camerasView.selected().id()};
		POST(url,  data, true);
	}

    function send_test_sms(general_data, show_notify) {
        var url = '/test_sms';
        POST(url, general_data, show_notify);
    }

    function GET(url, data, show_notify, callback, camera_id){
		var key = CookieUtils.readCookie("key");
		if (url.indexOf("?") == -1) {
			url = url + "?key=" + key;
		} else {
			url = url + "&key=" + key; 
		}
        $.ajax(DB_SERVER_URL + url, {type: "GET", dataType: "json",
            success: function (data, textStatus, jqXHR) {
                if(data['message'] === "OK" && data['error_code'] === 0){
                    if(show_notify){notify("Data saved successfully", "success");}
                    callback(data, camera_id);
                }
                else{
                    notify("Received invalid data from server, please try later again", "success");
                    console.log("Received invalid data from server, please try later again", data);
                }
            },
            error: function (data, textStatus, jqXHR) {
                notify("Server error", "error");
                console.log("server error: ", data)
            }

        }).fail(function() {
            notify("Server unavailable !!!", "error");
        });
    }

    function POST(url, data, show_notify){
		var key = CookieUtils.readCookie("key");

        if (url.indexOf("?") == -1) {
            url = url + "?key=" + key;
        } else {
            url = url + "&key=" + key; 
        }

        $.ajax(DB_SERVER_URL + url, {type: "POST", dataType: "json", data: data,
            success: function (data, textStatus, jqXHR) {
                if(data['message'] === "OK" && data['error_code'] === 0){
                    //callback(data['segments'], camera_id);
                    if(show_notify){notify("Data saved successfully", "success");}
                    return data;
                }
                else{
                    notify("Received invalid data from server, please try later again", "success");
                    console.log("Received invalid data from server, please try later again", data);
                }
            },
            error: function (data, textStatus, jqXHR) {
                notify("Server error", "error");
                console.log("server error: ", data)
            }
        }).fail(function() {
            notify("Server unavailable !!!", "error");
        });
    }

    return{
		set_general_account_params: set_general_account_params,
        get_user_cameras: get_user_cameras,
        get_data_for_camera: get_data_for_camera,
        get_general_camera_params: get_general_camera_params,
        set_general_camera_params: set_general_camera_params,
        get_camera_settings:get_camera_settings,
        set_camera_on_off: set_camera_on_off,
	    delete_camera_confirmed: delete_camera_confirmed,
		send_test_sms: send_test_sms
    }
})();

function notify(msg, type) {
//    notif({
//        type: type,
//        msg: msg,
//        position: "center",
//        opacity: 0.8
//    });
}
