var unsortableColumns = [];
$('#datatable-table').find('thead th').each(function(){
    if ($(this).hasClass( 'no-sort')){
        unsortableColumns.push({"bSortable": false});
    }
    else if ($(this).hasClass( 'numeric-comma-sort')){
        unsortableColumns.push({"sType": "numeric-comma"});
    } else {
        unsortableColumns.push(null);
    }
});

var selected = {}
var count = 0

$("#datatable-table").dataTable({
    "sDom": "<'row table-top-control'<'col-md-6 per-page-selector'l<'action-toolbar'>>r>t<'row table-bottom-control'<'col-md-6'i><'col-md-6'p>>",
    "oLanguage": {
        "sLengthMenu": "_MENU_ &nbsp; records per page"
    },
    "iDisplayLength": 50,
    "aaSorting": [[ 4, "desc" ]],
    "aoColumns": unsortableColumns,
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": "/dashboard/media/ajax",
    "fnDrawCallback": function( oSettings ) {
        $(".iCheck").iCheck({
            checkboxClass: 'icheckbox_square-grey',
            radioClass: 'iradio_square-grey'
        });
        $(".select-action").on("ifChecked", function() {
            selected[$(this).data('id')] = true
            console.log('add', $(this).data('id'), selected, d3.keys(selected))
            count += 1
            $("div.action-toolbar").show()
        })
        $(".select-action").on("ifUnchecked", function() {
            delete selected[$(this).data('id')]
            console.log('del', $(this).data('id'), selected, d3.keys(selected))
            count -= 1
            if (count <= 0) {
                $("div.action-toolbar").hide()
            }
        })
    }
});

$(".dataTables_length select").select2({
    minimumResultsForSearch: 10
});

$("div.action-toolbar").html('&nbsp;&nbsp;<button type="button" class="btn btn-danger delete-btn">Delete</button>');
$("div.action-toolbar").hide()

$(".delete-btn").click(function() {
    var submited = d3.keys(selected).length
    var done = 0
    d3.keys(selected).forEach(function(id) {
        console.log('Delete', id)
        $.ajax({
            type: "POST",
            url: '/remove_video_from_user',
            data: { video_id: id, admin_mode: true },
            success: function(resp) {
                console.log('resp', resp)
                if (resp.result) {
                    done += 1
                    if (done == submited) {
                        location.reload();
                    }
                }
                else {
                    console.log('error', resp, resp.message)
                    $("#alert-zone").append('\
<div class="alert alert-danger">\
    <button type="button" class="close" data-dismiss="alert">×</button>\
    <strong><i class="fa fa-ban"></i>  '+resp.error_code+'</strong> - '+resp.message+'\
</div>\
                    ')
                }
            },
            dataType: "json"
        });
    })
}) 



