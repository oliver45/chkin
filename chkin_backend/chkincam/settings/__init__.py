# settings module itself should return local settings so that
# manage.py commands work.
try:
    from .local import *
except ImportError, e:
    e.args = tuple(['%s (did you copy settings/local.example.py to settings/local.py?)' % e.args[0]])

    raise e
