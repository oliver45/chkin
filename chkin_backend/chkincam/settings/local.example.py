from .base import *

# SECURITY WARNING: keep the secret key used in production secret!
# SECRET_KEY = ''

DEBUG = True
THUMBNAIL_DEBUG = DEBUG

# COMPRESS_ENABLED = True

# ALLOWED_HOSTS = ['127.0.0.1']

INSTALLED_APPS += (
    # 'django-debug-toolbar',
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'db.sqlite3',
        'HOST': 'localhost',
        'USER': '',
        'PASSWORD': '',
        'PORT': ''
    }
}

# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

EMAIL_HOST = 'localhost'
EMAIL_HOST_USER = 'root'
EMAIL_HOST_PASSWORD = ''
EMAIL_PORT = 25
EMAIL_USE_TLS = False


# CACHES = {
#     'default': {
#         'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
#         'LOCATION': '127.0.0.1:11211',
#     }
# }


COMPRESS_PRECOMPILERS = (
    ('text/x-scss', 'django_libsass.SassCompiler'),
    # ('text/x-scss', 'django_pyscss.compressor.DjangoScssFilter'),
    # ('text/x-scss', 'sass --style compressed {infile} {outfile}'),
)

SESSION_KEY_ROTATION_TIME_INTERVAL = 1800
SERVER_SENDER_ADDRESS = "ChkInCam Support <chkincam@gmail.com>"
SERVER_HOSTNAME = ("www.chkincam.com")
SERVER_PORT = ("80")
STORAGE_LOCATION = ("/work/records")
HARTBEAT_TIME_INTERVAL = 10 * 60
HARTBEAT_PARAMETER_NAME = "last_hartbeat_at"
ONLINE_PARAMETER_NAME = "online"
SERVER_SENDER_ADDRESS = "ChkInCam Support <chkincam@gmail.com>"
GCM_ACTION_KEY_PASSWORD_CHANGED = "app.chkin.PASSWORD_CHANGED"
GCM_PASSWORD_KEY = "pw"
FRONTENT_URL = "http://192.168.0.5:8090/"
GEOIP_PATH = BASE_DIR
GEOIP_COUNTRY = 'GeoIP.dat'
GEOIP_CITY = 'GeoLiteCity.dat'
FRONTEND_URL = "http://www.chkincam.com/"


# https://github.com/ottoyiu/django-cors-headers
# CORS_ORIGIN_ALLOW_ALL = True
CORS_ORIGIN_WHITELIST = (
    # 'google.com',
    # 'hostname.example.com'
)
CORS_ORIGIN_REGEX_WHITELIST = (
    # '^(https?://)?(\w+\.)?google\.com$',
)

# redis://:password@hostname:port/db_number
# BROKER_URL = 'redis://localhost:6379/0'

# RabbitMQ
# BROKER_URL = 'amqp://guest:guest@localhost:5672//'
