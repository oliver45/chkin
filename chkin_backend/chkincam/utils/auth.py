import random
from uuid import uuid4
from django.conf import settings
from django.utils import timezone
from chkincam.utils import datetime_to_seconds
from logging import debug as d, info as i, warning as w, error as e, critical as c


def generate_challange():
    return '%030x' % random.randrange(16 ** 30)


def generate_session_key():
    return str(uuid4())


def generate_claim_camera_token():
    return str(uuid4())


def check_credential_session_key_rotation(auth):
    "Return True in case new session key have been generated, False otheerwise"
    ## check if key rotation is needed
    session_created = auth.session_created
    ## current session start time in seconds
    current_session_started = datetime_to_seconds(session_created)
    current_time = datetime_to_seconds(timezone.now())
    time_diff = current_time - current_session_started
    i("")
    i("Session keys timedifference in seconds : " + str(time_diff) +
      " session key rotation time interval : " + str(settings.SESSION_KEY_ROTATION_TIME_INTERVAL))
    i("")
    if (time_diff > settings.SESSION_KEY_ROTATION_TIME_INTERVAL):
        i("")
        i(" Rotating session key for username : " + auth.name)
        i("")
        ## generate new session key
        auth.session_key = generate_session_key()
        auth.challange = generate_challange()
        auth.session_created = timezone.now()
        auth.save()
        return True
    else:
        return False