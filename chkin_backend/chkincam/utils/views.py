import base64
from logging import debug as d, info as i, warning as w, error as e, critical as c
from celery.task.http import URL
from datetime import datetime
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from django.contrib.sites.models import Site
from django.views.generic import View
from django.http import JsonResponse, HttpResponseRedirect
from chkincam.utils.auth import check_credential_session_key_rotation
from chkincam.utils.exceptions import ErrorConvention
from apps.camera.models import Camera
from apps.users.models import PaymentRecord, User

ERROR_CODES = {
	"OK": 0,
	"NOT REGISTERED": 1,
	"BAD EMAIL": 2,
	"NOT APPLICABLE": 3,  # camera action performed from user
	"MISSING LINK": 4,  # comera key is missing !!!!
	"CAMERA ALREADY CLAIMED": 5,

	"CAMERA DELETED": 6,
	"USER DELETED": 7,

	"IDENTITY ERROR": 8,
	"CREDENTIALS MISSING": 9,
	"DUPLICATE": 10,
	"BROKEN LINK": 11,  # also in case you access foreign camera
	"MALFORMED_JSON": 12,
	"PAYMENT_CALCULATION_ISSUE": 13,
	"CAPTCHA VERIFICATION FAILED": 14,
	"SESSION NOT FOUND": 15,
	"NOT SUBSCRIBED": 16,
	"ZERO LENGTH VIDEO": 17,
}


class ChkincamResponseMixin(object):
	@staticmethod
	def ok():
		return JsonResponse({"result": True, "message": "OK", "error_code": 0})

	@staticmethod
	def error(message):
		w(message)
		return JsonResponse({"result": False, "message": message, "error_code": ERROR_CODES.get(message, -1)})

	@staticmethod
	def serve_result(obj):
		obj.update({"result": True, "message": "OK", "error_code": 0})
		return JsonResponse(obj)


class AuthViewMixin(object):
	"""
	Mixin intended to be used in combination with django.views.generic.View.
	Implements user or camera authorization on every request
	"""
	auth = None
	auth_model = None

	def dispatch(self, request, *args, **kwargs):
		self.auth = self._authorize(request)

		return super(AuthViewMixin, self).dispatch(request, *args, **kwargs)

	def _authorize(self, request):
		auth_obj = None
		if "key" in request.COOKIES:
			_key = request.COOKIES.get("key")
			d(" --- auth --- key={0}".format(_key))
		else:
			if "key" in request.REQUEST:
				_key = request.REQUEST.get("key")
				d(" --- auth --- key={0}".format(_key))
			elif "PayerID" in request.REQUEST:
				if "token" in request.REQUEST:
					token = request.REQUEST.get('token')
					query = PaymentRecord.objects.get(token=token)
					auth_obj = query.owner
					_key = auth_obj.session_key
			else:
				raise ErrorConvention('CREDENTIALS MISSING')

		if _key == "" and auth_obj == None:
			raise ErrorConvention('IDENTITY ERROR')

		# auth_obj = self.get_auth_model().objects.filter(session_key=_key).first()
		if not auth_obj:
			auth_obj = self.get_auth_object(_key)

		if not auth_obj:
			## all_credentials = Credentials.query().get()
			"""
			print "\n\n " , "could not authorize key : " , _key + " DB returned : " , auth , \
					" , all credentials : " , all_credentials , "\n\n"
			"""
			raise ErrorConvention('IDENTITY ERROR')

		if (auth_obj.session_created is None):
			auth_obj.session_created = datetime.now()
			auth_obj.save()
			return auth_obj

		if check_credential_session_key_rotation(auth_obj):
			raise ErrorConvention('IDENTITY ERROR')
		else:
			return auth_obj

	def get_auth_model(self):
		return self.auth_model

	def get_auth_object(self, session_key):
		return self.get_auth_model().objects.filter(session_key=session_key).first()


class UserAuthView(AuthViewMixin, ChkincamResponseMixin, View):
	"""
	Class that implements user authorization on every request
	"""
	# auth_model = settings.AUTH_USER_MODEL

	def get_auth_model(self):
		return get_user_model()


class CameraAuthView(AuthViewMixin, ChkincamResponseMixin, View):
	"""
	Class that implements camera authorization on every request
	"""
	auth_model = Camera


class UniversalAuthView(AuthViewMixin, ChkincamResponseMixin, View):
	"""
	Class that implements EITHER camera OR user authorization on every request
	"""

	def get_auth_object(self, session_key):
		try:
			result = get_user_model().objects.get(session_key=session_key)
		except get_user_model().DoesNotExist:
			try:
				result = Camera.objects.get(session_key=session_key)
			except Camera.DoesNotExist:
				result = None

		return result


class ClaimDeviceMixin(object):

	def _claim_device(self, user, id, email, camera):
		if not user or not isinstance(user, get_user_model()):
			# raise Exception("MISSING LINK")
			e("missing link for id={id} owner={o}".format(id=id, o=email))
			return HttpResponseRedirect('user:expired')# bleffing: it is users mistake :o
		camera.owner = user
		camera.user = user.name

		# taskqueue.add(url='/dashboard/users/camera_increment?key=%s' % user.pk, method='GET')


		## TODO uncomment bellow when dashboard become available !!!
		"""
		ip = settings.SERVER_HOSTNAME + ":" + settings.SERVER_PORT;
		task_result = URL('http://{ip}/dashboard/users/media_increment'.format(
			ip=ip)
		).get_async(key=base64.urlsafe_b64encode(str(user.pk)))
		"""

		camera.claim_token = None
		camera.save()

		return HttpResponseRedirect(reverse('home'))
