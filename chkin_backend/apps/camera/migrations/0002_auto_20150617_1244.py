# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('statistics', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('camera', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='videosegment',
            name='user',
            field=models.ForeignKey(related_name='video_segment', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='gcmcommand',
            name='camera',
            field=models.ForeignKey(related_name='gcm_command', to='camera.Camera'),
        ),
        migrations.AddField(
            model_name='cameraparameter',
            name='camera',
            field=models.ForeignKey(related_name='parameter_values', to='camera.Camera'),
        ),
        migrations.AddField(
            model_name='camera',
            name='owner',
            field=models.ForeignKey(related_name='camera_owner', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='camera',
            name='usage',
            field=models.ForeignKey(related_name='camera', blank=True, to='statistics.Usage', null=True),
        ),
    ]
