# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Camera',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('claim_token', models.CharField(max_length=128, null=True, blank=True)),
                ('build_id', models.CharField(max_length=45)),
                ('name', models.CharField(max_length=45)),
                ('user', models.CharField(max_length=45, null=True, blank=True)),
                ('deleted', models.BooleanField(default=False)),
                ('creation_date', models.DateTimeField(auto_now=True)),
                ('challenge', models.CharField(max_length=45, blank=True)),
                ('session_key', models.CharField(max_length=100, blank=True)),
                ('session_created', models.DateTimeField(null=True, blank=True)),
                ('password', models.CharField(max_length=100, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='CameraParameter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=256, blank=True)),
                ('name', models.CharField(max_length=45)),
            ],
        ),
        migrations.CreateModel(
            name='GCMCommand',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('utc_expiry', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='VideoSegment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('utc_start', models.DateTimeField()),
                ('duration', models.FloatField()),
                ('chkin_version', models.CharField(max_length=45)),
                ('video_width', models.CharField(max_length=45)),
                ('video_height', models.CharField(max_length=45)),
                ('title', models.CharField(max_length=45)),
                ('description', models.TextField()),
                ('video_name', models.CharField(max_length=45)),
                ('video_url', models.CharField(max_length=256, blank=True)),
                ('thumbnail_url', models.CharField(max_length=256, blank=True)),
                ('thumbnail_width', models.CharField(max_length=45)),
                ('thumbnail_height', models.CharField(max_length=45)),
                ('camera', models.CharField(max_length=45)),
                ('username', models.CharField(max_length=45)),
                ('size', models.IntegerField()),
                ('deleted', models.BooleanField(default=False)),
                ('owner', models.ForeignKey(related_name='video_segment_owner', to='camera.Camera')),
            ],
            options={
                'ordering': ['-utc_start'],
            },
        ),
    ]
