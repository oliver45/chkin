from django.conf.urls import url
from apps.users.views import UserView, ChallengeView, AuthView, ClientLoggedInView, ClientLoggedOutView, GetUserInfo, \
	UsernameAvailable, UserSubscription, PayCanceled, PayConfirmed, Pay, DeleteUser, \
	UploadNewRelease, RegisterUser, Key, ChangeUserPassword, SessionExpired, NumberOfConnectedClients, \
	ChosePass, Expired, DeleteUserTask, SendCommandToClient, MobileSubscribe

urlpatterns = [
	url(r'^user$', UserView.as_view(), name='user_view'),
	url(r'^chl', ChallengeView.as_view(), name='users_challenge'),
	url(r'^auth', AuthView.as_view(), name='users_auth'),
	url(r'^client_logged_in', ClientLoggedInView.as_view(), name='users_client_logged_in'),
	url(r'^client_logged_out', ClientLoggedOutView.as_view(), name='users_client_logged_out'),
	url(r'^(?P<pk>\d+)/$', UserView.as_view(), name='user_view'),
	url(r'^chose-pass$',ChosePass.as_view(), name='choose_pass'),
	url(r'^expired/$', Expired.as_view(), name='expired'),
	url(r'^get_user_info/$', GetUserInfo.as_view(), name='get_user_info'),
	url(r'^username_available/$', UsernameAvailable.as_view(), name='username_available'),
	url(r'^user_subscription/$', UserSubscription.as_view(), name='user_subscription'),
	url(r'^user_view/$', UserView.as_view(), name='user_view'),
	url(r'^pay_canceled/$', PayCanceled.as_view(), name='pay_canceled'),
	url(r'^pay_confirmed/$', PayConfirmed.as_view(), name='pay_confirmed'),
	url(r'^mobile_subscribe/$', MobileSubscribe.as_view(), name='pay_confirmed'),
	url(r'^pay/$', Pay.as_view(), name='pay'),
	url(r'^delete_user_task/$', DeleteUserTask.as_view(), name='delete_user_task'),
	url(r'^delete_user/$', DeleteUser.as_view(), name='delete_user'),
	url(r'^upload_new_release/$', UploadNewRelease.as_view(), name='request_update_upload_url'),
	url(r'^register_user/$', RegisterUser.as_view(), name='register_user'),
	url(r'^key/$', Key.as_view(), name='key'),
	url(r'^change_user_password/$', ChangeUserPassword.as_view(), name='change_user_password'),
	url(r'^session_expired/$', SessionExpired.as_view(), name='session_expired'),
	url(r'^number_of_connected_clients/$', NumberOfConnectedClients.as_view(), name='number_of_connected_clients'),
	url(r'^send_command_to_client/$', SendCommandToClient.as_view(), name='send_command_to_client'),
]
