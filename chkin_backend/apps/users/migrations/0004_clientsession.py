# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_usercommand'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClientSession',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user_agent', models.CharField(max_length=45)),
                ('session_key', models.CharField(max_length=100, blank=True)),
                ('expires_at', models.DateTimeField()),
                ('ip_address', models.CharField(max_length=30, blank=True)),
                ('logged_in', models.DateTimeField()),
                ('logged_out', models.DateTimeField()),
                ('owner', models.ForeignKey(related_name='client_session', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
