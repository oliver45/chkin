# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import annoying.fields


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20150622_1036'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserCommand',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('utc_expiry', models.DateTimeField()),
                ('payload', annoying.fields.JSONField(null=True, blank=True)),
                ('user', models.ForeignKey(related_name='gcm_command', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
