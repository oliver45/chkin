# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.auth.models
import django.utils.timezone
from django.conf import settings
import django.core.validators
import annoying.fields


class Migration(migrations.Migration):

    dependencies = [
        ('statistics', '0001_initial'),
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, max_length=30, validators=[django.core.validators.RegexValidator('^[\\w.@+-]+$', 'Enter a valid username. This value may contain only letters, numbers and @/./+/-/_ characters.', 'invalid')], help_text='Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.', unique=True, verbose_name='username')),
                ('first_name', models.CharField(max_length=30, verbose_name='first name', blank=True)),
                ('last_name', models.CharField(max_length=30, verbose_name='last name', blank=True)),
                ('email', models.EmailField(max_length=254, verbose_name='email address', blank=True)),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('subscription_start_time', models.DateTimeField(null=True, blank=True)),
                ('subscription_end_time', models.DateTimeField(null=True, blank=True)),
                ('phone_number', models.CharField(max_length=20, blank=True)),
                ('telephone_co', models.CharField(max_length=45, blank=True)),
                ('camera_count', models.IntegerField(null=True, blank=True)),
                ('media_count', models.IntegerField(null=True, blank=True)),
                ('storage_used', models.IntegerField(null=True, blank=True)),
                ('deleted', models.BooleanField(default=False)),
                ('challenge', models.CharField(max_length=45, blank=True)),
                ('session_key', models.CharField(max_length=100, blank=True)),
                ('session_created', models.DateTimeField(null=True, blank=True)),
                ('groups', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', verbose_name='groups')),
            ],
            options={
                'abstract': False,
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Packet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=45, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='PaymentRecord',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('token', models.CharField(max_length=45)),
                ('start', models.DateTimeField()),
                ('end', models.DateTimeField()),
                ('price', models.FloatField()),
                ('plan_type', models.CharField(max_length=45)),
                ('plan_days', models.IntegerField()),
                ('checkout_setup_response', annoying.fields.JSONField(null=True, blank=True)),
                ('transaction_details_response', annoying.fields.JSONField(null=True, blank=True)),
                ('execution_response', annoying.fields.JSONField(null=True, blank=True)),
                ('owner', models.ForeignKey(related_name='payment_record_by_user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Subscription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('packets', models.ForeignKey(related_name='subscription', blank=True, to='users.Packet')),
            ],
        ),
        migrations.AddField(
            model_name='user',
            name='subscription',
            field=models.ForeignKey(related_name='user_subscription', blank=True, to='users.Subscription', null=True),
        ),
        migrations.AddField(
            model_name='user',
            name='usage',
            field=models.ForeignKey(related_name='user_usage', blank=True, to='statistics.Usage', null=True),
        ),
        migrations.AddField(
            model_name='user',
            name='user_permissions',
            field=models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions'),
        ),
    ]
