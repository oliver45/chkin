import os
import time
from json import dumps
from logging import info as i, warning as w, error as e
import md5
from datetime import datetime, timedelta

from annoying.functions import get_object_or_None
from django.conf import settings
from django.contrib.auth import login, logout
from django.core.urlresolvers import reverse
from django.utils import timezone
from validate_email import validate_email
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic import View
from django.contrib.auth import get_user_model
from django.views.generic import DetailView, TemplateView

from apps.camera.models import Camera, VideoSegment
from apps.users.models import User, Packet, Subscription, PaymentRecord, ClientSession
from apps.users.utils import _pal
from chkincam.utils.auth import generate_challange
from chkincam.utils.views import ChkincamResponseMixin, UserAuthView, UniversalAuthView, CameraAuthView
from chkincam.utils import generate_session_key, check_credential_session_key_rotation
from apps.android.models import ChkinAPK
from chkincam.utils.storage import storageManager
import paypalrestsdk

paypalrestsdk.configure({
	"mode": "sandbox", # sandbox or live
  	"client_id": "AWJDycg09-0DGCiJGLwzOuDYBC1iqW4zTGtTfmjdYZAZjiCdWu88ocsUv9Gtk0Rr7m2RHOu3DdMWBQJu",
  	"client_secret": "EIo--4c74zxNo1ufi6lUCtr5wy2BbeZZ9dcrMT-1EtPVr97f7G__Dv7wexrGsymazqTVZOzZJ8aw1a-f" })

class UserView(DetailView):
	template_name = 'users/user.html'
	model = get_user_model()

	def get_object(self, queryset=None):
		return self.request.user


class ChallengeView(View, ChkincamResponseMixin):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(ChallengeView, self).dispatch(request, *args, **kwargs)

		name = request.REQUEST.get('name')
		i("	name={0}".format(name))
		# # all names should be case insenssitive
		name = str(name).lower()
		try:
			auth = get_user_model().objects.get(email=name)
		except get_user_model().DoesNotExist:
			try:
				auth = Camera.objects.get(name=name)
			except Camera.DoesNotExist:
				return self.error("NOT REGISTERED")

		return self.serve_result({'chl': auth.challenge})


class AuthView(View, ChkincamResponseMixin):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		# # all names should be case insenssitive
		name = request.REQUEST.get('name')
		response = request.REQUEST.get('response')
		i("	name={0}".format(name))
		name = str(name).lower()

		try:
			auth = get_user_model().objects.get(email=name)
		except get_user_model().DoesNotExist:
			try:
				auth = Camera.objects.get(name=name)
			except Camera.DoesNotExist:
				return self.error("NOT REGISTERED")

		raw = auth.password + auth.challenge
		actual = md5.new(raw).hexdigest()
		i("sent={0} actual={1} ch={2} pas={3}, raw={4}".format(response, actual, auth.challenge, auth.password,
															   raw))
		if actual == response:
			if not auth.session_key:
				auth.session_key = generate_session_key()
				auth.session_created = datetime.now()
				auth.save()
			else:
				check_credential_session_key_rotation(auth)
			return self.serve_result({"key": auth.session_key})
		else:
			return self.error('IDENTITY ERROR')


class ClientLoggedInView(UserAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(ClientLoggedInView, self).dispatch(request, *args, **kwargs)

		user = self.auth
		if user is None:
			return self.error("NOT REGISTERED")
		if not isinstance(user, get_user_model()):
			return self.error("NOT APPLICABLE")

		user_agent = request.META.get('HTTP_USER_AGENT', 'Unknown')

		session = ClientSession()
		session.owner = user
		session.logged_in = datetime.fromtimestamp(float(time.time()))
		session.ip_address = request.session.ip
		# assuming that incoming key and session_key are same.
		session.session_key = user.session_key
		session.expires_at = user.session_created + timedelta(0, settings.SESSION_KEY_ROTATION_TIME_INTERVAL)
		session.user_agent = user_agent
		session.save()

		return self.ok()


class ClientLoggedOutView(UserAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(ClientLoggedOutView, self).dispatch(request, *args, **kwargs)

		user = self.auth
		if user is None:
			return self.error("NOT REGISTERED")
		if not isinstance(user, get_user_model()):
			return self.error("NOT APPLICABLE")

		user_agent = request.META.get('HTTP_USER_AGENT', 'Unknown')

		session = ClientSession.objects.filter(user_agent=user_agent, owner=user, logged_out=None).order_by("-logged_out")

		if (len(session) == 0):
			return self.error("SESSION NOT FOUND")

		session = session[0]

		session.logged_out = datetime.fromtimestamp(float(time.time()))
		session.save()
		return self.ok()


class ChosePass(TemplateView):
	template_name = 'users/chose_pass.html'


class Expired(TemplateView):
	template_name = 'users/expired.html'


class GetUserInfo(UserAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(GetUserInfo, self).dispatch(request, *args, **kwargs)

		user = self.auth
		if not isinstance(user, User):
			return self.error("NOT APPLICABLE")
		ret = {
			"name": user.name,
			"phone_number": user.phone_number,
			"telephone_co": user.telephone_co,
			"camera_count": user.camera_count,
			"media_count": user.media_count,
			"storage_used": user.storage_used
		}
		if (user.subscription_start_time != None):
			ret["subscription_start_time"] = user.subscription_start_time.strftime("%Y-%m-%d %H:%M:%S")
		if (user.subscription_end_time != None):
			ret["subscription_end_time"] = user.subscription_end_time.strftime("%Y-%m-%d %H:%M:%S")
		# if (user.registered_on != None):
		# ret["registered_on"] = user.registered_on.strftime("%Y-%m-%d %H:%M:%S")

		return self.serve_result({"state": ret})


class UsernameAvailable(View, ChkincamResponseMixin):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(UsernameAvailable, self).dispatch(request, *args, **kwargs)

		email = str(request.REQUEST.get('email')).lower()
		if not validate_email(email):
			return self.error("BAD EMAIL")
		user = get_object_or_None(get_user_model(), email=email)
		if user:
			return self.error('DUPLICATE')
		return self.ok()


class UserSubscription(UniversalAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(UserSubscription, self).dispatch(request, *args, **kwargs)
	
		user = None;
		if isinstance(self.auth,  User):
			user = self.auth;
		else:
			user = self.auth.owner;

		subscription = "false"
		now = timezone.now()

		if user.subscription_end_time is not None:
			if user.subscription_end_time < now:
				subscription = "false"
			else:
				subscription = "true"
		else:
			subscription = "false"

		return self.serve_result({"subscription": subscription})


class PayCanceled(UserAuthView):
	http_method_names = ['get']

	def get(self, request, *args, **kwargs):
		i("you lousy bugger !")

		return HttpResponseRedirect(reverse("users:user_view"))


# class pay_confirmed(self, PayerID, token, **kwargs):


class PayConfirmed(UserAuthView):
	http_method_names = ['get']


	def get(self, request, *args, **kwargs):

		e("pay_confirmed, do_express_checkout_payment() with paymentaction = 'Sale'. This will finalize and bill.")
		pal = _pal(self)
		token = request.GET.get('token')
		details_response = pal.get_express_checkout_details(token=token)
		query = PaymentRecord.objects.get(token=token)
		#payment = query.get()
		user = query.owner
		query.transaction_details_response = details_response.raw
		query.save()
		# # {u'TAXAMT': [u'0.00'],
		# #  u'CORRELATIONID': [u'1d9431a917a66'],
		# #  u'SHIPTOSTATE': [u'CA'],
		# #  u'EMAIL': [u'testclient@chkincam.com'],
		##  u'PAYMENTREQUEST_0_INSURANCEAMT': [u'0.00'],
		##  u'PAYMENTREQUEST_0_SHIPTOZIP': [u'95131'],
		##  u'CHECKOUTSTATUS': [u'PaymentActionNotInitiated'],
		##  u'SHIPTOCITY': [u'San Jose'],
		##  u'CURRENCYCODE': [u'USD'],
		##  u'SHIPTOCOUNTRYCODE': [u'US'],
		##  u'PAYMENTREQUEST_0_SHIPTOSTATE': [u'CA'],
		##  u'VERSION': [u'98.0'],
		##  u'PAYMENTREQUEST_0_CURRENCYCODE': [u'USD'],
		##  u'PAYMENTREQUEST_0_SHIPPINGAMT': [u'0.00'],
		##  u'BILLINGAGREEMENTACCEPTEDSTATUS': [u'0'],
		##  u'PAYMENTREQUEST_0_SHIPTONAME': [u'alex alex'],
		##  u'PAYMENTREQUEST_0_AMT': [u'10.00'],
		##  u'PAYMENTREQUEST_0_ADDRESSNORMALIZATIONSTATUS': [u'None'],
		##  u'SHIPDISCAMT': [u'0.00'],
		##  u'PAYMENTREQUEST_0_SHIPTOCOUNTRYNAME': [u'United States'],
		##  u'TIMESTAMP': [u'2014-07-17T14:21:17Z'],
		##  u'PAYERSTATUS': [u'verified'],
		##  u'LASTNAME': [u'alex'],
		##  u'SHIPTONAME': [u'alex alex'],
		##  u'ADDRESSSTATUS': [u'Confirmed'],
		##  u'COUNTRYCODE': [u'US'],
		##  u'AMT': [u'10.00'],
		##  u'PAYMENTREQUEST_0_TAXAMT': [u'0.00'],
		##  u'SHIPTOCOUNTRYNAME': [u'United States'],
		##  u'INSURANCEAMT': [u'0.00'],
		##  u'TOKEN': [u'EC-5EM85976686342813'],
		##  u'PAYMENTREQUESTINFO_0_ERRORCODE': [u'0'],
		##  u'PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE': [u'US'],
		##  u'PAYMENTREQUEST_0_INSURANCEOPTIONOFFERED': [u'false'],
		##  u'SHIPPINGAMT': [u'0.00'],
		##  u'BUILD': [u'11843215'],
		##  u'ACK': [u'Success'],
		##  u'PAYERID': [u'ABMTAF6HN3RVG'],
		##  u'SHIPTOZIP': [u'95131'],
		##  u'PAYMENTREQUEST_0_SHIPTOSTREET': [u'1 Main St'],
		##  u'PAYMENTREQUEST_0_HANDLINGAMT': [u'0.00'],
		##  u'PAYMENTREQUEST_0_ADDRESSSTATUS': [u'Confirmed'],
		##  u'FIRSTNAME': [u'alex'],
		##  u'PAYMENTREQUEST_0_SHIPTOCITY': [u'San Jose'],
		##  u'HANDLINGAMT': [u'0.00'],
		##  u'PAYMENTREQUEST_0_SHIPDISCAMT': [u'0.00'],
		##  u'SHIPTOSTREET': [u'1 Main St']}
		response = pal.do_express_checkout_payment(
			token=token,
			paymentaction="Sale",
			payerid=details_response.payerid,
			amt=details_response.amt,
		)
		query.execution_response = response.raw
		query.save()
		### https://developer.paypal.com/docs/classic/api/merchant/DoExpressCheckoutPayment_API_Operation_NVP/
		## {u'PAYMENTINFO_0_FEEAMT': [u'0.59'],
		##  u'PAYMENTINFO_0_TRANSACTIONTYPE': [u'expresscheckout'],
		##  u'TOKEN': [u'EC-55039542R0257530S'],
		##  u'PAYMENTINFO_0_ORDERTIME': [u'2014-07-17T14:24:55Z'],
		##  u'REASONCODE': [u'None'],
		##  u'PAYMENTINFO_0_PAYMENTTYPE': [u'instant'],
		##  u'PAYMENTINFO_0_CURRENCYCODE': [u'USD'],
		##  u'PAYMENTINFO_0_PROTECTIONELIGIBILITY': [u'Eligible'],
		##  u'TRANSACTIONTYPE': [u'expresscheckout'],
		##  u'PAYMENTINFO_0_PROTECTIONELIGIBILITYTYPE': [u'ItemNotReceivedEligible,UnauthorizedPaymentEligible'],
		##  u'TRANSACTIONID': [u'04W209885E478981K'],
		##  u'PROTECTIONELIGIBILITY': [u'Eligible'],
		##  u'SUCCESSPAGEREDIRECTREQUESTED': [u'false'],
		##  u'PAYMENTINFO_0_ERRORCODE': [u'0'],
		##  u'CORRELATIONID': [u'53b075967cc0'],
		##  u'TIMESTAMP': [u'2014-07-17T14:24:55Z'],
		##  u'PAYMENTINFO_0_ACK': [u'Success'],
		##  u'TAXAMT': [u'0.00'],
		##  u'PAYMENTINFO_0_PENDINGREASON': [u'None'],
		##  u'INSURANCEOPTIONSELECTED': [u'false'],
		##  u'PAYMENTTYPE': [u'instant'],
		##  u'PAYMENTSTATUS': [u'Completed'],
		##  u'PAYMENTINFO_0_PAYMENTSTATUS': [u'Completed'],
		##  u'PAYMENTINFO_0_TAXAMT': [u'0.00'],
		##  u'FEEAMT': [u'0.59'],
		##  u'ACK': [u'Success'],
		##  u'CURRENCYCODE': [u'USD'],
		##  u'PAYMENTINFO_0_REASONCODE': [u'None'],
		##  u'VERSION': [u'98.0'],
		##  u'ORDERTIME': [u'2014-07-17T14:24:55Z'],
		##  u'AMT': [u'10.00'],
		##  u'PAYMENTINFO_0_AMT': [u'10.00'],
		##  u'PENDINGREASON': [u'None'],
		##  u'PAYMENTINFO_0_TRANSACTIONID': [u'04W209885E478981K'],
		##  u'BUILD': [u'11843215'],
		##  u'PAYMENTINFO_0_SECUREMERCHANTACCOUNTID': [u'RL3LLB75U5TDC'],
		##  u'SHIPPINGOPTIONISDEFAULT': [u'false']}
		if response["PAYMENTINFO_0_ACK"] == u'Success' and response["PAYMENTINFO_0_PAYMENTSTATUS"] == u'Completed':
			# increase subscription date
			now = datetime.now(timezone.utc)
			w("[PAYMENT] subscription before payment start={0} end={1} token={2}".format(user.subscription_start_time,
																						 user.subscription_end_time,
																						 token))

			if user.subscription_end_time is None or user.subscription_start_time is None or user.subscription_end_time < now:
				user.subscription_start_time = now
				user.subscription_end_time = now + timedelta(days=query.plan_days)
				user.save()
			else:  #elif user.subscription_end_time > now:
				# just prolong it
				user.subscription_end_time += timedelta(days=query.plan_days)
				user.save()
			w("[PAYMENT] subscription after payment start={0} end={1} token={2}".format(user.subscription_start_time,
																						user.subscription_end_time,
																						token))
			# Success
		redirect_url="{0}".format(settings.FRONTEND_URL)
		return HttpResponseRedirect( redirect_url )
		#return HttpResponseRedirect(reverse("users:user_view"))
		# something went wrong
		e("[PAYMENT] ERROR: something went wrong check this manually")
		# return HttpResponseRedirect(reverse("users:user_view"))

	EXAMPLE_USER_EMAIL = "testclient@chkincam.com"


class Pay(UserAuthView):
	http_method_names = ['get']

	def get(self, request, *args, **kwargs):

		now = datetime.now()
		price = 0
		days = 0
		description = ""
		start = now
		plan = request.GET.get('plan')
		if plan == "month":
			days = 30
			price = 10.00
			description = "Monthly plan $10.00"
		elif plan == "year":
			days = 365
			price = 100.00
			description = "Yearly plan $100.00"

		end = start + timedelta(days=days)
		# end = start + timedelta(days=1)

		e("[PAYMENT] request pay {0} plan price={1}".format(plan, price))

		auth = self._authorize(request)

		#user = auth.get()
		if not isinstance(auth, User):
			e("[PAYMENT] not authorized as user, name={0} plan={1} end={2}".format(auth.name, plan))
			# TODO: Render HTML instead of json response
			return self.error("NOT APPLICABLE")

		# contact paypal and redirect user to payment form
		pal = _pal(self)
		response = pal.set_express_checkout(
			PAYMENTREQUEST_0_AMT=str(price),
			PAYMENTREQUEST_0_PAYMENTACTION='Order',
			PAYMENTREQUEST_0_DESC=description,  # amt=str(price),  #paymentaction='Order',  #desc=description,
			returnurl="http://{host}:{port}/pay_confirmed".format(host=settings.SERVER_HOSTNAME,
																  port=settings.SERVER_PORT),
			cancelurl="http://{host}:{port}/pay_canceled".format(host=settings.SERVER_HOSTNAME,
																 port=settings.SERVER_PORT),
			email=auth.name
		)
		assert response is not None, "response is None"
		payment = PaymentRecord()
		payment.token = response.token
		payment.start = start
		payment.end = end
		payment.price = price
		payment.plan_type = plan
		payment.plan_days = days
		payment.checkout_setup_response = response.raw
		#payment.owner = auth.key
		payment.owner = auth
		payment.save()

		# # inspect response.raw dict:
		# #		{u'VERSION': [u'98.0'],
		# #		 u'TOKEN': [u'EC-2H501994NU741124E'],
		##		 u'BUILD': [u'11843215'],
		##		 u'CORRELATIONID': [u'a25d1539f189a'],
		##		 u'TIMESTAMP': [u'2014-07-17T14:13:53Z'],
		##		 u'ACK': [u'Success']}
		# Redirect your client to this URL for approval.
		redirect_url = pal.generate_express_checkout_redirect_url(response.token)

		#raise cherrypy.HTTPRedirect(redirect_url)
		return HttpResponseRedirect( redirect_url )


class DeleteUserTask(CameraAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(DeleteUserTask, self).dispatch(request, *args, **kwargs)

		user = get_user_model().objects.get(pk=request.REQUEST.get('user_key'))
		for camera in Camera.objects.filter(owner=user):
					for media in VideoSegment.objects.filter(owner=camera):
						if media.video_url != None:
							media.video_url.delete()
						if media.thumbnail_url != None:
							media.thumbnail_url.delete()
						media.deleted = True
						media.save()
		return self.ok()


class DeleteUser(CameraAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(DeleteUser, self).dispatch(request, *args, **kwargs)
		email = str(request.REQUEST.get('email')).lower()
		user = get_user_model().objects.get(email=email)
		if user is None:
			return self.error("NOT REGISTERED")
		if not isinstance(user, User):
			return self.error("NOT APPLICABLE")

		if not request.REQUEST.get('admin_mode'):
			super(DeleteUser, self).dispatch(request, *args, **kwargs)
			if user != self.auth.owner:
				# you must own that camera
				return self.error("IDENTITY ERROR")
		else:
			guser = request.user
			if not guser or not guser.is_staff:
				# you must be an admin
				return self.error("IDENTITY ERROR")

		# taskqueue.add(url='/delete_user_task', params={'user_key': (user.key.urlsafe())})
		# task_result = URL('http://{domain}/delete_user_task'.format(
		#	 domain=Site.objects.get_current().domain)
		# ).get_async(user_key=base64.urlsafe_b64encode(str(user.pk)))

		# "deleting" the user's cameras
		for camera in Camera.objects.filter(owner=user):
			camera.deleted = True
			camera.save()

		# "deleting" user
		# user_auth.key.delete()
		user.camera_count = 0
		user.media_count = 0
		user.storage_used = 0
		user.deleted = True
		user.save()
		return self.ok()


# class request_update_upload_url(self, **kwargs):
class UploadNewRelease(UserAuthView):
	http_method_names = ['get']

	def dispatch(self, request, *args, **kwargs):
		super(UploadNewRelease, self).dispatch(request, *args, **kwargs)

		record = ChkinAPK()
		record.version = request.REQUEST.get('chkin_version')
		record.build_id = request.REQUEST.get('build_id')
		record.uploaded = datetime.fromtimestamp(time.time())

		if request.FILES.has_key("apk"):
			record.apk_url = self.savefileToDisk("apk", request)

		if request.FILES.has_key("dump"):
			record.dump_url = self.savefileToDisk("dump", request)

		if request.FILES.has_key("seeds"):
			record.seeds_url = self.savefileToDisk("seeds", request)

		if request.FILES.has_key("mapping"):
			record.mapping_url = self.savefileToDisk("mapping", request)
			

		if request.FILES.has_key("usage"):
			record.usage_url = self.savefileToDisk("usage", request)
		record.save();
		return self.ok()

	def savefileToDisk(self, fileName, request):
		file_url = storageManager.generate_file_location(self.auth.email)
		_file = open(os.path.join(settings.STORAGE_LOCATION, file_url), "w")
		data = request.FILES[fileName]
		for chunk in data.chunks():
			_file.write(chunk)
		return file_url
			


# for debug only
# def register_user(self, name, password):


class RegisterUser(View, ChkincamResponseMixin):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(RegisterUser, self).dispatch(request, *args, **kwargs)
		email = str(request.REQUEST.get('name')).lower()
		password = str(request.REQUEST.get('password'))

		# check if name i valid email format
		if not validate_email(email):
			return self.error("BAD EMAIL")
		# TODO: send confirmation email

		## check for duplicate name
		if get_object_or_None(get_user_model(), email=email):
			return self.error('DUPLICATE')

		user = get_user_model()()
		user.email = email
		user.username = email

		# for testing in chkincamdev.com server
		## set developer subscription by default for testing pourposes
		packet = get_object_or_None(Packet, name="Developer")
		if packet is None:
			packet = Packet()
			packet.name = "Developer"
			packet.save()
		subscriptions = Subscription.objects.all()
		subscription = None
		if not subscriptions.exists():
			subscription = Subscription()
			subscription.packets = packet;
			subscription.save()
		else:
			for s in subscriptions:
				# @TODO should be m2m relationship between Packet and Subscription
				if s.packets.name == "Developer":
					subscription = s

		user.subscription = subscription
		user.subscription_start_time = datetime.utcnow()
		subscription_duration_in_seconds = 60 * 60 * 24 * 365 ## one year
		user.subscription_end_time = datetime.utcnow() + timedelta(0, subscription_duration_in_seconds)

		user.date_joined = datetime.utcnow()
		user.password = password
		user.challenge = generate_challange()
		user.save()

		return self.ok()

		# def key(self):


class Key(UserAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(Key, self).dispatch(request, *args, **kwargs)

		# return str(dir(cherrypy.request.cookie["key"]))
		if request.COOKIES.get('key'):
			return HttpResponse(request.COOKIES.get('key'))
		else:
			return HttpResponseRedirect(reverse("home"))


			# class change_user_password(self, key, password):


class ChangeUserPassword(UserAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(ChangeUserPassword, self).dispatch(request, *args, **kwargs)
		user = self.auth

		if user is None:
			return self.error("NOT REGISTERED")
		if not isinstance(user, User):
			return self.error("NOT APPLICABLE")
		# # change password
		user.password = request.GET.get('password')
		user.save()
		## Notify al devices that password have changed
		cameras = Camera.objects.filter(owner=user)
		payload = dumps({
			"action": settings.GCM_ACTION_KEY_PASSWORD_CHANGED,
			settings.GCM_PASSWORD_KEY: request.GET.get('password')
		})
		 # TODO: send_gcm_message must be fixed for a django(now for googe app engine)
		# for camera in cameras:
			# camera.send_gcm_message(payload,  settings.GCM_PASSWORD_CHANGED_COLLAPSE_KEY, -1)
		return self.ok()


# def session_expired(self):
class SessionExpired(UniversalAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(SessionExpired, self).dispatch(request, *args, **kwargs)
		return self.ok()


class NumberOfConnectedClients(UserAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(NumberOfConnectedClients, self).dispatch(request, *args, **kwargs)

		user = self.auth
		if user is None:
			return self.error("NOT REGISTERED")
		if not isinstance(user, get_user_model()):
			return self.error("NOT APPLICABLE")


		## sessions = user.session_set.all()
		## sessions = user.session_set.objects.filter(owner=user and logged_out=None).order_by("-logged_in")
		
		sessions = ClientSession.objects.filter(owner=user, logged_out=None).order_by("-logged_in")

		result = []
		for session in sessions:
			if session.expires_at >= timezone.now():
				session_as_dict = {}
				session_as_dict["ip_address"] = session.ip_address
				# session_as_dict["logged_in"] = calendar.timegm(session.logged_in.timetuple())
				session_as_dict["client"] = session.user_agent
				result.append(session_as_dict)
			else:
				# we need to do some more improvements on log out management
				session.logged_out = datetime.now()
				session.save()

		# check for last session ip
		last_session = sessions[0]
		last_session_ip = last_session.ip_address if last_session else None
		return self.serve_result({"count": len(result) or 1, "sessions": result, "last_session_ip": last_session_ip})

class SendCommandToClient(CameraAuthView):
	"""
		Arguments required :
		camera session key as key
		time to live as expire_in_seconds
		collapse key as collapse_key, not used currently
		message payload in json format as payload
	"""
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(SendCommandToClient, self).dispatch(request, *args, **kwargs)
		
		expiry_in_seconds = 300
		if request.REQUEST.has_key('expiry_in_seconds'):
			expiry_in_seconds = float(request.REQUEST.get('expiry_in_seconds'))

		collapse_key = None
		
		if not request.REQUEST.has_key('payload'):
			return self.error("NOT APPLICABLE")

		camera = self.auth
		user = camera.owner

		if camera.owner.id != user.id:
			# you must own that camera
			return self.error("IDENTITY ERROR")

		e("send_command to client: {0}, payload : {1}".format(user.email, repr(request.REQUEST.get('payload'))))
		user.send_gcm_message(request.REQUEST.get('payload'), collapse_key, expiry_in_seconds)
		return self.ok()

class MobileSubscribe(CameraAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(MobileSubscribe, self).dispatch(request, *args, **kwargs)
		user = self.auth
		paymentId = request.REQUEST.get('paymentId')
		if (paymentId is None):
			self.error("INVALID PAYMENT ID")
		plan = request.REQUEST.get('plan')
		plan_days = 30
		if ( plan == "monthly"):
			plan_days = 30
		elif ( plan == "yearly" ):
			plan_days = 365
		else:
			return self.error("INVALID PLAN")

		try:
			payment = paypalrestsdk.Payment.find(paymentId)
			response = payment.to_dict()
			if response['state'] == "approved":
				payamt = (int)(0 + response['transactions'][0]['amount']['total'])
				now = datetime.now(timezone.utc)
				if ( (payamt == 10 and plan_days == 30) or (payamt == 100 and plan_days == 365) ):
					if user.subscription_end_time is None or user.subscription_start_time is None or user.subscription_end_time < now:
						user.subscription_start_time = now
						user.subscription_end_time = now + timedelta(days=plan_days)
						user.save()
					else:
						user.subscription_end_time += timedelta(days=plan_days)
						user.save()
					return self.serve_result({"subscribe_endtime": user.subscription_end_time})
				else:
					return self.error("INVALID AMOUNT")

		except Exception as err:
			return self.error("INVALID PAYMENT ID")

		return self.error("NOT APPLICABLE")