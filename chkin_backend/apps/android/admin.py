from django.contrib import admin
from apps.android.models import ChkinAPK
from apps.camera.models import GCMCommand


admin.site.register(GCMCommand)
admin.site.register(ChkinAPK)
