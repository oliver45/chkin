from django.db import models


class ChkinAPK(models.Model):
    ## String, format : major.minor version
    version = models.CharField(max_length=45)
    uploaded = models.DateTimeField()
    apk_url = models.CharField(max_length=256, blank=True)
    dump_url = models.CharField(max_length=256, blank=True)
    mapping_url = models.CharField(max_length=256, blank=True)
    seeds_url = models.CharField(max_length=256, blank=True)
    usage_url = models.CharField(max_length=256, blank=True)
    build_id = models.CharField(max_length=45)
