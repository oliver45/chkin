from django.conf.urls import url
from apps.android.views import WhatIsTheLatestApkVersion, DownloadLatestApk, SendGcmToClient

urlpatterns = [
	url(r'^what_is_the_latest_apk_version/$', WhatIsTheLatestApkVersion.as_view(), name='what_is_the_latest_apk_version'),
	url(r'^download_latest_apk/$', DownloadLatestApk.as_view(), name='download_latest_apk'),
	url(r'^send_gcm_to_client/$', SendGcmToClient.as_view(), name='downlosend_gcm_to_clientad_latest_apk'),
]
