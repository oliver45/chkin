from django.conf.urls import url
from apps.dashboard.views import DashboardView, UserlistView, BlobMissingView, \
	CameralistView, FilmsView, StatsCountsView, StatsHistogramView, UsersListAPI, DeleteUserAPI, CSVAPI
''''
urlpatterns = [
	url(r'^dashboard/$', DashboardView.as_view(), name='dashboard_view'),
	url(r'^dashboard/stats/$', DashboardView.as_view(), name='dashboard_stats'),
	url(r'^dashboard/stats/counts$', StatsCountsView.as_view(), name='dashboard_st_count'),
	url(r'^dashboard/usage/histogram$', StatsHistogramView.as_view(), name='dashboard_st_histogram'),
	url(r'^dashboard/users/$', UserlistView.as_view(), name='dashboard_users'),
	url(r'^dashboard/users/list$', UsersListAPI.as_view(), name='dashboard_users_list'),
	url(r'^dashboard/delete_user$', DeleteUserAPI.as_view(), name='dashboard_users_delete'),
	url(r'^dashboard/csv/(.*?)', CSVAPI.as_view(), name='dashboard_users_csv'),
	url(r'^dashboard/cameras/$', CameralistView.as_view(), name='dashboard_cameras'),
	url(r'^dashboard/films/$', FilmsView.as_view(), name='dashboard_media'),
	url(r'^dashboard/media/ajax$', FilmsView.as_view(), name='dashboard_media'),
	url(r'^dashboard/blob_missing/$', BlobMissingView.as_view(), name='dashboard_blobmissing'),
]
'''''