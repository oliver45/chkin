/// <reference path="../jquery/jquery.d.ts" />
/// <reference path="../moment/moment.d.ts" />
/// <reference path="../vis_js/vis.d.ts" />

class BTL{

	private cursor: HTMLElement;
	private items : beeItem[] = [];
	private vis_options: any = {
		margin: { item: 10 },
		height: 70,
		zoomable: false,
		'margin.axis': 2,
		minHeight: 50,
		showMajorLabels : false,
		stack: false,
		selectable: true,
		showCurrentTime: false,
		maxHeight: "45px"
	};

	private element: HTMLElement;
	private timeSpan: TimeSpan;
	private timeline: any;

	private cursor_screen_x: number = 0; // px
	private time_per_pixel: number = 0;
	private timelineUpdated: boolean = false;
	private moveInProgress: boolean = false;
	private outByUser: boolean = false;
	private margin: number = 4;
	private treshold: number = 10;
	private initDone: boolean = false;

	constructor (element: HTMLElement, timeSpan: TimeSpan){
		this.element = element;
		this.timeSpan = timeSpan;
		this.init();
	}

	private init(): void{
		this.items = [];
		var cursor = new beeItem(
			"cursor",
			"<div id='cursor_content' class='cursor_content'><p></p></div>",
			new Date(2014, 10, 10),
			new Date(2014, 10, 10),
			"cursor"
		);
		this.items.push(cursor);

		// Create a Timeline
		this.timeline = new vis.Timeline(this.element, this.items, this.vis_options);
		this.RegisterEvents();
		this.SetCursorTime(moment().toDate());
		this.SetTimeSpan(this.timeSpan.SPAN, this.timeSpan.COUNT);
		setTimeout(() => {
			this.SetCursorTime(moment().toDate());
		}, 620);

		this.initDone = true;
	}

	public SetCursorTime(time: Date, update: boolean = false): void{
		if(time === undefined || time === null || !this.isValidDate(time)){ return;}
		var updateCursor = false;
		if(!this.moveInProgress){
			updateCursor = true;
		}else if(this.moveInProgress && update){
			updateCursor = true;
		}

		if(updateCursor){
			console.log("this.moveInProgress: ", this.moveInProgress, "update: ", update)
			if(!this.outByUser){this.CheckCursorPosition()};
			var dataSet = this.timeline.itemsData;
			var cursor = dataSet.get("cursor");
			cursor.start = time;
			cursor.end = time;
			dataSet.update(cursor);
			$("#cursor_content>p").html( moment(time).format("hh:mm A") );
			$("#cursor_content").on("mousedown", (e) => this.onMouseTimelineDown(e,this));
		}
	}

	public GetCursorTime(): Date{
		var dataSet = this.timeline.itemsData;
		var cursor = dataSet.get("cursor");
		return cursor.start;
	}

	public AddItems(items: beeItem[]): void{
		var addList : beeItem[] = [];
		var dataSet = this.timeline.itemsData;
		var presentItems = dataSet.getIds();
		items.forEach(function(item) {
			if(jQuery.inArray( item.id, presentItems ) > -1){ dataSet.update(item); }else{ addList.push(item); }
		});
		dataSet.add(addList);
	}
	
	public RemoveItems(): void{
		var dataSet = this.timeline.itemsData;
		var cursor = dataSet.get("cursor");

		dataSet.clear();
		dataSet.add(cursor);
	}

	public SetTimeSpan(span: SpanType, count: number): void{
		var end;
		var start;
		var spanData = {};
		spanData[span["str"]] = count;
		var duration = moment.duration(spanData);
		var treshhold = (duration.asSeconds()/100) * this.treshold;
		if(!this.initDone){
			end = moment();
			start = moment(end).subtract(count, span["str"]).add(treshhold, "seconds");
			end = moment(start).add(count, span["str"]);
		}else{
			start = this.timeline.getWindow().start;
			end = this.timeline.getWindow().end;
			var cursor = this.GetCursorTime();
			if(cursor > start && cursor < end){
				end = cursor;
				start = moment(end).subtract(count, span["str"]).add(treshhold, "seconds");
				end = moment(start).add(count, span["str"]);
			}else{
				end = moment(this.timeline.getWindow().end);
				start = moment(end).subtract(count, span["str"]);
			}
		}
		this.timeline.setWindow(start.toDate(), end.toDate());

		var w = $(this.element).width();
		this.time_per_pixel = duration.asMilliseconds() / w;
	}

	public GetTimeBorders(): Date[]{
		return [ this.timeline.getWindow().start, this.timeline.getWindow().end];
	}

	public GetStageTimeLength(): number{
		return moment.duration(moment(this.timeline.getWindow().end).diff(moment(this.timeline.getWindow().start))).asMilliseconds();
	}

	public PixelToTime(dif_pixel: number):number{
		var time = dif_pixel * this.time_per_pixel;
		return time;
	}

	public MoveCursorBy(pixel: number):void{
		var time = this.PixelToTime(pixel);
		var currentCursor = this.GetCursorTime();
		var t = moment(currentCursor).add(time, "milliseconds");
		console.log("MoveCursorBy");
		this.SetCursorTime(t.toDate(), true);
	}

	public CheckCursorPosition(): void{
		var start = this.timeline.getWindow().start;
		var end = this.timeline.getWindow().end;
		if(this.GetCursorTime() >= moment(end).subtract(this.PixelToTime(this.margin), "milliseconds").toDate()) {
			var dur = moment.duration(moment(end).diff(moment(start)));
			var treshhold = (dur.asSeconds() / 100) * this.treshold;
			var new_end = moment(end).add(treshhold, "seconds");
			var new_start = moment(start).add(treshhold, "seconds");
			this.timeline.setWindow(new_start, new_end);
		}
	}

	/**
	 * Move start/end so cursor place will be according to treshold value
	 * @constructor
	 */
	public MoveWindowToCursor(time: Date): void{
		time = time === undefined ? this.GetCursorTime() : time;
		var start = this.timeline.getWindow().start;
		var end = this.timeline.getWindow().end;
		var dur = moment.duration(moment(end).diff(moment(start)));
		var treshhold = (dur.asSeconds() / 100) * this.treshold;
		var new_end = moment(this.GetCursorTime()).add(treshhold, "seconds");
		var new_start = moment(new_end).subtract(dur.asSeconds(), "seconds");
		this.timeline.setWindow(new_start.toDate(), new_end.toDate());
	}

	private isValidDate(time: Date): boolean{
		if (isNaN(time.getTime())==false) { return true; }
		return false;
	}

	private RegisterEvents(): void{
		$("#cursor_content").on("mousedown", (e) => this.onMouseTimelineDown(e, this));

		$(document).on("MoveCursorBy", (e: any) =>{
			var diff = e["px"] - this.cursor_screen_x;
			this.cursor_screen_x = e["px"];
			this.MoveCursorBy(diff);
		});

		$("div.vispanel.center").on("mousedown", (e) => {
			this.timelineUpdated = false;
		});

		$("div.vispanel.center").on("mouseup", (e) => {
			if(this.timelineUpdated){ return;}
			var offset = $("div.vispanel.center").offset();
			var diff = e.pageX - offset.left;
			var start = this.timeline.getWindow().start;
			var time = moment(start).add(this.PixelToTime(diff - this.margin), "milliseconds");
			this.SetCursorTime(time.toDate());
			var event = jQuery.Event( "cursorChange" );
			event["time"] = this.GetCursorTime();
			$(document).trigger(event);
		});

		this.timeline.on("rangechange", (e: any) =>{
			this.timelineUpdated = true;
			$("#cursor_content>p").html( moment(this.GetCursorTime()).format("hh:mm A") );
			$(document).trigger(jQuery.Event( "timelineTimeChange" ));
		});

		this.timeline.on("rangechanged", (e: any) =>{
			$(document).trigger(jQuery.Event( "timelineTimeChange" ));
			var cursorTime = this.GetCursorTime();
			if(cursorTime < this.timeline.getWindow().start
				|| cursorTime > this.timeline.getWindow().end){
				this.outByUser = true;
			}
			else{
				this.outByUser = false;
			}
		});
	}

	private onMouseUp(e: any, _this: any): void{
		_this.moveInProgress = false;
		console.log("moveInProgress", _this.moveInProgress);
		$(document).off("mouseup");
		$(document).off("mousemove");
		var event = jQuery.Event( "cursorChange" );
		event["time"] = _this.GetCursorTime();
		$(document).trigger(event);
	}

	private onMouseMove(e: any): void{
		var event = jQuery.Event( "MoveCursorBy" );
		event["px"] = e.pageX;
		$(document).trigger(event);
	}

	private onMouseTimelineDown(e: any, _this: any): void{
		e.stopPropagation();
		$(document).on("mouseup", (e) => _this.onMouseUp(e, _this));
		$(document).on("mousemove", _this.onMouseMove);
		_this.moveInProgress = true;
		console.log("moveInProgress", _this.moveInProgress);
		_this.cursor_screen_x = e.pageX;
	}
}

/**
 * BTL time span unit
 */
class TimeSpan{
	constructor(public SPAN: SpanType, public COUNT: number){}
}

/**
 * Alternative for enum (IDE problem)
 */
class SpanType{
	public static MINUTE={str:"minute", id:1000 * 60};
	public static HOUR={str:"hour", id:1000 * 60 * 60};
	public static DAY={str:"day", id: 1000 * 60 * 60 * 24};
	public static MONTH={str:"month", id: 1000 * 60 * 60 * 24 * 30};
}

class beeItem{
	constructor(
		public id: string,
		public content: string,
		public start: Date,
		public end: Date,
		public className? : string
		){}
}